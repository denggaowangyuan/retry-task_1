package com.retry.task.core.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/4  10:56
 * @Description TODO
 */
public class RetryTaskContext implements Serializable {
    private static final long serialVersionUID = 2858937096357504300L;

    private Long taskId;

    private Long instanceId;

    private Integer isTest;

    /**
     * 所使用的项目名称
     */
    private String projectName;
    /**
     * 重试任务的描述
     */
    private String taskDesc;

    /**
     * 重试任务的bean 名称
     */
    private String taskName;

    /**
     * 类型 0 重试任务 默认是重试任务
     * 1 定时任务
     */
    private int taskType;

    /**
     * 是否异步 0 异步
     * 1 同步
     */
    private int isAsync;

    /**
     * 是否广播 0单机执行
     * 1 广播
     */
    private int isBroadcast;

    private String creatorName;

    private String creatorId;

    /**
     * 异常通知人
     */
    private String warningPerson;

    /**
     * 通知类型
     */
    private String warningType;

    private String parameters;

    private String creatorIp;

    private String token;

    private String traceId;

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Long instanceId) {
        this.instanceId = instanceId;
    }

    public Integer getIsTest() {
        return isTest;
    }

    public void setIsTest(Integer isTest) {
        this.isTest = isTest;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public int getTaskType() {
        return taskType;
    }

    public void setTaskType(int taskType) {
        this.taskType = taskType;
    }

    public int getIsAsync() {
        return isAsync;
    }

    public void setIsAsync(int isAsync) {
        this.isAsync = isAsync;
    }

    public int getIsBroadcast() {
        return isBroadcast;
    }

    public void setIsBroadcast(int isBroadcast) {
        this.isBroadcast = isBroadcast;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getWarningPerson() {
        return warningPerson;
    }

    public void setWarningPerson(String warningPerson) {
        this.warningPerson = warningPerson;
    }

    public String getWarningType() {
        return warningType;
    }

    public void setWarningType(String warningType) {
        this.warningType = warningType;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getCreatorIp() {
        return creatorIp;
    }

    public void setCreatorIp(String creatorIp) {
        this.creatorIp = creatorIp;
    }
}
