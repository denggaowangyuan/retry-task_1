package com.retry.task.core.utils.threadPool;

import java.util.Map;

import org.slf4j.MDC;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2021/9/10  10:26 上午
 * @Description TODO
 */
public class RetryTaskLogRunable<T> implements Runnable {

    private final Map<String, String> mdcContextMap;

    private static ThreadLocal<Object> THREAD_LOCAL = new ThreadLocal<>();

    public static Object getContext() {
        return THREAD_LOCAL.get();
    }

    private final Runnable iCallBack;

    public RetryTaskLogRunable(Map<String, String> context, Runnable callBack) {
        if (callBack == null) {
            throw new IllegalArgumentException("callBack can not be null");
        }
        this.mdcContextMap = context;
        this.iCallBack = callBack;
    }

    @Override
    public void run() {
        try {
            if (mdcContextMap != null) {
                MDC.setContextMap(mdcContextMap);
            }
            iCallBack.run();
        } finally {
            MDC.clear();
        }
    }

}
