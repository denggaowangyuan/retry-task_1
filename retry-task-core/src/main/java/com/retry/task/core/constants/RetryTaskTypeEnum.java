package com.retry.task.core.constants;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/2  11:45
 * @Description TODO
 */
public enum RetryTaskTypeEnum {

    RETRY_TASK(1,"重试任务"),
    SCHEDULER_JOB(2,"定时调度任务");
    private int type;

    private String desc;

    RetryTaskTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
