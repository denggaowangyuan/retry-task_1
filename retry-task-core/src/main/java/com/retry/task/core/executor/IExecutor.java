package com.retry.task.core.executor;

import com.retry.task.core.model.HeartDTO;
import com.retry.task.core.model.RetryTaskContext;
import com.retry.task.core.model.RetryTaskDTO;
import com.retry.task.core.model.RetryTaskLogDTO;
import com.retry.task.core.model.base.Result;

/**
 * Created by xuxueli on 17/3/1.
 */
public interface IExecutor {

    /**
     * 心跳注册
     *
     * @return
     */
    Result<String> heart(HeartDTO heartDTO);

    /**
     * 任务执行
     *
     * @return
     */
    Result<String> run(RetryTaskContext context);

    /**
     * 终止任务
     *
     * @return
     */
    Result<String> killTask();

    /**
     * 任务回调
     *
     * @return
     */
    Result<String> callBack();

    /**
     * 创建重试任务
     *
     * @param retryTaskDTO
     * @return
     */
    Result<Long> createRetryTask(RetryTaskDTO retryTaskDTO);

    /**
     * 创建定时任务
     *
     * @param retryTaskDTO
     * @return
     */
    Result<String> createSchedulerTask(RetryTaskDTO retryTaskDTO);

    Result<String> updateRetryTaskLog(RetryTaskLogDTO retryTaskLogDTO);
}
