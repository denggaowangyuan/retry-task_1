package com.retry.task.core.model.base;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/1  16:06
 * @Description TODO
 */
public class PageResult<T> extends Result<T> {

    private Integer totalCount;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public static <T> PageResult<T> getSuccess(T t) {
        PageResult<T> resultDTO = new PageResult<>();
        resultDTO.setIsSuccess(true);
        resultDTO.setData(t);
        return resultDTO;
    }

    public static <T> PageResult<T> getFail(String code, String errMsg) {
        PageResult<T> resultDTO = new PageResult<>();
        resultDTO.setIsSuccess(false);
        resultDTO.setCode(code);
        resultDTO.setErrMsg(errMsg);
        return resultDTO;
    }
}
