package com.retry.task.core.constants;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2021/11/11  7:25 下午
 * @Description TODO
 */
public enum RetryTaskStatusEnum {
    SEND_FAIL(-9, "发送消息失败"),
    TO_START(0, "待执行"),
    PROGRESS(1, "进行中"),
    SUCESS(2, "成功"),
    FAIL(-1, "失败"),
    TIME_OUT_NO_MESSAGE(-2, "超时失败，未收到消息"),
    TIME_OUT_NO_UPDATE(-3, "超时,收到消息未能更新日志状态"),
    TIME_OUT_NO_CALLBACK(-5, "超时，回调失败"),
    ;
    int code;
    String desc;

    RetryTaskStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static RetryTaskStatusEnum get(int code) {
        RetryTaskStatusEnum[] statusEnumArr = RetryTaskStatusEnum.values();
        for (RetryTaskStatusEnum statusEnum : statusEnumArr) {
            if (statusEnum.getCode() == code) {
                return statusEnum;
            }
        }
        return null;
    }
}
