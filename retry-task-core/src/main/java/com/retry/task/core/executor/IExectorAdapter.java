package com.retry.task.core.executor;

import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpMethod;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/23  10:16
 * @Description TODO
 */
public interface IExectorAdapter {

    /**
     * String requestData = msg.content().toString(CharsetUtil.UTF_8);
     * String uri = msg.uri();
     * HttpMethod httpMethod = msg.method();
     *
     * @param
     * @return
     */
    Object processRequest(String requestData, String uri, HttpMethod httpMethod);
}
