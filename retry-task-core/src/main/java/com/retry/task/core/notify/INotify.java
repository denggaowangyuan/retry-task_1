package com.retry.task.core.notify;

import com.retry.task.core.model.RetryTaskContext;
import com.retry.task.core.model.RetryTaskLogDTO;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/4  10:57
 * @Description TODO
 */
public interface INotify {

    void notify(RetryTaskContext context, RetryTaskLogDTO taskLogDTO);
}
