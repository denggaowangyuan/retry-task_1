package com.retry.task.core.constants;

public interface RetryTaskConstants {
    String PREFIX = "spring.retry.task";

    String ENABLED = PREFIX + ".enabled";

}