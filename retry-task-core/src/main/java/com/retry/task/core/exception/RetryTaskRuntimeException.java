package com.retry.task.core.exception;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/25  22:16
 * @Description TODO
 */
public class RetryTaskRuntimeException extends RuntimeException{

    public RetryTaskRuntimeException() {
        super();
    }

    public RetryTaskRuntimeException(String message) {
        super(message);
    }

    public RetryTaskRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public RetryTaskRuntimeException(Throwable cause) {
        super(cause);
    }

    protected RetryTaskRuntimeException(String message, Throwable cause, boolean enableSuppression,
        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
