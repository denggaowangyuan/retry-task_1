package com.retry.task.core.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/18  21:28
 * @Description TODO
 */
public class ThrowableUtils {

    public static String toString(Throwable e) {
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        String errorMsg = stringWriter.toString();
        return errorMsg;
    }
}
