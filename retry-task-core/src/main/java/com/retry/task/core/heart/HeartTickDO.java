package com.retry.task.core.heart;

import java.util.Date;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/25  22:22
 * @Description TODO
 */
public class HeartTickDO {

    /**
     * serverIp
     */
    private String serverIp;

    /**
     * 心跳的次数
     */
    private long heartNum;

    /**
     * 上次触发时间
     */
    private Date lastHeartTime;

    private Date lastEndHeartTime;



    /**
     * 上次心跳的结果
     */
    private Integer result;

    /**
     * 上次心跳工时
     */
    private long  timeCost;

    public Date getLastEndHeartTime() {
        return lastEndHeartTime;
    }

    public void setLastEndHeartTime(Date lastEndHeartTime) {
        this.lastEndHeartTime = lastEndHeartTime;
    }

    public long getTimeCost() {
        return timeCost;
    }

    public void setTimeCost(long timeCost) {
        this.timeCost = timeCost;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public long getHeartNum() {
        return heartNum;
    }

    public void setHeartNum(long heartNum) {
        this.heartNum = heartNum;
    }

    public Date getLastHeartTime() {
        return lastHeartTime;
    }

    public void setLastHeartTime(Date lastHeartTime) {
        this.lastHeartTime = lastHeartTime;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
}
