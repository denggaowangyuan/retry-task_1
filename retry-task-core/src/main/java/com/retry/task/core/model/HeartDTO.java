package com.retry.task.core.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/26  12:06
 * @Description TODO
 */
public class HeartDTO extends BaseDTO implements Serializable {

    private static final long serialVersionUID = 7519706794141295363L;
    private String clientIp;

    private Integer isTest;

    private String projectName;

    private String token;

    private Integer executeType;



    public Integer getIsTest() {
        return isTest;
    }

    public void setIsTest(Integer isTest) {
        this.isTest = isTest;
    }

    public Integer getExecuteType() {
        return executeType;
    }

    public void setExecuteType(Integer executeType) {
        this.executeType = executeType;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }


}
