package com.retry.task.core.constants;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/3  20:42
 * @Description TODO
 */
public enum RetryTaskExecuteTypeEnum {
    INTERVAL(0, "按照间隔时间"),

    CRON(1, "corn 表达式");
    private Integer type;

    private String desc;

    RetryTaskExecuteTypeEnum(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static RetryTaskExecuteTypeEnum get(int type) {
        RetryTaskExecuteTypeEnum[] typeEnums = RetryTaskExecuteTypeEnum.values();
        for (RetryTaskExecuteTypeEnum typeEnum : typeEnums) {
            if (type == typeEnum.getType()) {
                return typeEnum;
            }
        }
        return null;
    }

}
