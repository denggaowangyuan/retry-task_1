package com.retry.task.core.utils.threadPool;

import java.util.concurrent.Callable;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2021/11/3  3:36 下午
 * @Description TODO
 */
public class RetryTaskCallable<T> implements Callable {

    private static ThreadLocal<Object> THREAD_LOCAL = new ThreadLocal<>();

    public static Object getContext() {
        return THREAD_LOCAL.get();
    }

    /**
     *
     */
    private final Object context;

    private final Callable<T> callable;

    public RetryTaskCallable(Object rpcContext, Callable<T> callable) {
        if (callable == null) {
            throw new IllegalArgumentException("callBack can not be null");
        }
        this.context = rpcContext;
        this.callable = callable;
    }

    @Override
    public Object call() throws Exception {

        try {
            if (context != null) {
                THREAD_LOCAL.set(context);
            }
            return callable.call();
        } finally {
            THREAD_LOCAL.remove();
        }
    }
}
