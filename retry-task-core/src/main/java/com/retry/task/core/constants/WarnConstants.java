package com.retry.task.core.constants;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/9/24  13:59
 * @Description TODO 告警类型
 */
public interface WarnConstants {


    String WARN = "warn";

    String DING_DING = "dingding";


    String PHONE = "phone";


    String EMAIL = "email";

}