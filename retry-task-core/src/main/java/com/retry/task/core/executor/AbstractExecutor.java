package com.retry.task.core.executor;

import com.retry.task.core.model.HeartDTO;
import com.retry.task.core.model.RetryTaskContext;
import com.retry.task.core.model.RetryTaskDTO;
import com.retry.task.core.model.RetryTaskLogDTO;
import com.retry.task.core.model.base.Result;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/4  23:30
 * @Description TODO
 */
public abstract class AbstractExecutor implements IExecutor {
    @Override
    public Result<String> heart(HeartDTO heartDTO) {
        return Result.getSuccess(null);
    }

    @Override
    public Result<String> run(RetryTaskContext context) {
        return Result.getSuccess(null);
    }

    @Override
    public Result<String> killTask() {
        return Result.getSuccess(null);
    }

    @Override
    public Result<String> callBack() {
        return Result.getSuccess(null);
    }

    @Override
    public Result<Long> createRetryTask(RetryTaskDTO retryTaskDTO) {
        return Result.getSuccess(null);
    }

    @Override
    public Result<String> createSchedulerTask(RetryTaskDTO retryTaskDTO) {
        return Result.getSuccess(null);
    }

    @Override
    public Result<String> updateRetryTaskLog(RetryTaskLogDTO retryTaskLogDTO) {
        return Result.getSuccess(null);
    }
}
