package com.retry.task.core.constants;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/28  13:36
 * @Description TODO
 */
public enum RetryTaskURIEnum {

    HEART("/heart", "心跳"),
    RUN("/run", "任务执行"),
    KILL("/kill", "任务终止"),
    CREATE_RETRY_TASK("/createRetryTask", "创建重试任务"),
    CALL_BACK("/callBack", "任务回调"),
    UPDATE_RETRY_TASK_LOG("/updateRetryTaskLog", "更新重试任务信息");

    private String uri;

    private String desc;

    public String getUri() {
        return uri;
    }

    public String getDesc() {
        return desc;
    }

    RetryTaskURIEnum(String uri, String desc) {
        this.uri = uri;
        this.desc = desc;
    }
}
