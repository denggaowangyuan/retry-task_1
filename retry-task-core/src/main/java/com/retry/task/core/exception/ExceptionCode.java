package com.retry.task.core.exception;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/7  11:15
 * @Description TODO
 */
public class ExceptionCode {

    public static final String COMMON_EXCEPTION = "100";

    public static final String SOCKET_TIME_OUT_EXCEPTION = "200";
    public static final String CONNECTION_EXCEPTION = "201";
    public static final String HTTP_EXCEPTION = "300";

    public static final String CAN_NOT_FIND_HEART_TICK = "301";
}
