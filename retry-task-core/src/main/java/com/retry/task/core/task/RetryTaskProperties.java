package com.retry.task.core.task;

import com.retry.task.core.constants.RetryTaskConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/25  13:44
 * @Description TODO
 */
@ConfigurationProperties(prefix = RetryTaskConstants.PREFIX)
public class RetryTaskProperties {

    /**
     * server 地址，
     */
    private String serverHost;

    /**
     *
     */
    private boolean enable;

    private int isTest;

    /**
     * 服务端url前缀
     */
    private String serverPre = "http://";

    /**
     * 执行端url前缀
     */
    private String clientPre = "http://";

    private String serverPort;

    private String clientPort;

    private String clientIp;

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getServerPre() {
        return serverPre;
    }

    public void setServerPre(String serverPre) {
        this.serverPre = serverPre;
    }

    public String getClientPre() {
        return clientPre;
    }

    public void setClientPre(String clientPre) {
        this.clientPre = clientPre;
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }

    public String getClientPort() {
        return clientPort;
    }

    public void setClientPort(String clientPort) {
        this.clientPort = clientPort;
    }

    public int getIsTest() {
        return isTest;
    }

    public void setIsTest(int isTest) {
        this.isTest = isTest;
    }

    /**
     * 接入项目名称
     */
    private String projectName;

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getServerHost() {
        return serverHost;
    }

    public void setServerHost(String serverHost) {
        this.serverHost = serverHost;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
