package com.retry.task.core.constants;

import org.apache.commons.lang3.StringUtils;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/12  13:37
 * @Description TODO
 */
public enum SelectorExectorEnum {
    RANDOM,//随机
    LAST_HEART,//最新一次心跳
    POLLING;//轮询

    public static SelectorExectorEnum getSelectorExector(String selectorType) {
        SelectorExectorEnum[] values = SelectorExectorEnum.values();
        for (SelectorExectorEnum tmpSelector : values) {
            if (StringUtils.equals(selectorType, tmpSelector.name())) {
                return tmpSelector;
            }
        }
        return null;
    }
}
