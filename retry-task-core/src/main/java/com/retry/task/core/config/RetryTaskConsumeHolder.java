package com.retry.task.core.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.retry.task.core.task.interfaces.IRetryTaskListener;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/4  22:19
 * @Description TODO
 */
public class RetryTaskConsumeHolder {
    private static final Map<String, IRetryTaskListener> LISTENER_MAP = new ConcurrentHashMap<>();

    private RetryTaskConsumeHolder() {

    }

    public synchronized static void addRetryTask(String name, IRetryTaskListener retryTaskListener) {
        LISTENER_MAP.put(name, retryTaskListener);
    }

    public static IRetryTaskListener getRetryTaskListener(String taskName) {
        return LISTENER_MAP.get(taskName);
    }
}
