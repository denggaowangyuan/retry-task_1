package com.retry.task.core.model.base;

import java.io.Serializable;

import com.google.gson.JsonObject;
import com.retry.task.core.utils.GsonTool;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/15  23:33
 * @Description TODO
 */
public class Result<T> implements Serializable {

    private static final long serialVersionUID = -9182342776833562452L;

    public static final String FAIL_CODE = "100";
    private T data;

    private Boolean isSuccess;

    private String errMsg;

    private String code;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean success) {
        isSuccess = success;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static <T> Result<T> getSuccess(T t) {
        Result<T> resultDTO = new Result<>();
        resultDTO.setIsSuccess(true);
        resultDTO.setData(t);
        return resultDTO;
    }

    public static <T> Result<T> getFail(String code, String errMsg) {
        Result<T> resultDTO = new Result<>();
        resultDTO.setIsSuccess(false);
        resultDTO.setCode(code);
        resultDTO.setErrMsg(errMsg);
        return resultDTO;
    }

    public static void main(String[] args) {
        Result result = Result.getFail("100", "200");

        String str = GsonTool.toJsonString(result);

        Result result1 = GsonTool.fromJson(str,Result.class);

        System.out.println(str);
    }

}
