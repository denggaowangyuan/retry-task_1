package com.retry.task.core.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public interface ApplicationStarted extends ApplicationListener<ContextRefreshedEvent> {
    Logger log = LoggerFactory.getLogger(ApplicationStarted.class);

    /**
     * Spring应用启动完成后的调用此方法
     */
    @Override
    default void onApplicationEvent(ContextRefreshedEvent event) {
        ApplicationContext applicationContext = event.getApplicationContext();
        if (applicationContext.getParent() == null) {
            this.doApplicationStarted();
            log.info("ApplicationStarted#doApplicationStarted is called: '{}'", this.getClass().getName());
        }
    }

    /**
     * Spring应用启动完成后的处理逻辑
     */
    void doApplicationStarted();
}
