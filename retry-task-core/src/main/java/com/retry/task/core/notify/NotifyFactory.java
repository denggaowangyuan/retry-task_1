package com.retry.task.core.notify;

import java.util.List;

import com.google.common.collect.Lists;
import com.retry.task.core.model.RetryTaskContext;
import com.retry.task.core.model.RetryTaskLogDTO;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/4  10:58
 * @Description TODO
 */
public class NotifyFactory {

    private static final List<INotify> notifyList = Lists.newArrayList();

    private NotifyFactory() {}

    public static void addNotify(INotify iNotify) {
        notifyList.add(iNotify);
    }

    public static void doNotify(RetryTaskContext context, RetryTaskLogDTO retryTaskLog) {
        for (INotify iNotify : notifyList) {
            iNotify.notify(context, retryTaskLog);
        }
    }
}
