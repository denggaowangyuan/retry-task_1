package com.retry.task.core.constants;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/2  22:01
 * @Description TODO
 */
public enum HeartResultEnum {
    SUCESS(1,"SUCESS"),FAIL(-1,"FAIL");
    private int code;
    private String desc;

    HeartResultEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
