package com.retry.task.core.heart;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/25  22:20
 * @Description TODO
 */
public class ServerHeartHolder {

    private static Map<String, HeartTickDO> HEART_MAP = new ConcurrentHashMap<>();

    public static void put(String serverIp, HeartTickDO heartTickDO) {
        if (serverIp == null) {
            return;
        }
        if (heartTickDO == null) {
            return;
        }
        HEART_MAP.put(serverIp, heartTickDO);
    }

    public static List<HeartTickDO> getServerHeartList() {
        List<HeartTickDO> heartTickDOList = Lists.newArrayList();

        HEART_MAP.forEach((key, value) -> {
            heartTickDOList.add(value);
        });
        return heartTickDOList;
    }

    public static HeartTickDO get(String serverIp) {
        if (serverIp == null) {
            throw new IllegalArgumentException("serverIp is null");
        }
        HeartTickDO heartTickDO = HEART_MAP.get(serverIp);
        if (heartTickDO == null) {
            heartTickDO = new HeartTickDO();
            put(serverIp, heartTickDO);
        }
        return heartTickDO;
    }
}
