package com.retry.task.core.constants;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/29  17:59
 * @Description TODO
 */
public enum RetryTaskServerType {

    SERVER(1,"服务器"),
    CLIENT(2,"执行器");

    private int type;

    private String desc;

    RetryTaskServerType(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
