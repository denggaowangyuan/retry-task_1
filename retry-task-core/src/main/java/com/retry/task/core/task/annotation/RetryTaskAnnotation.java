package com.retry.task.core.task.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * @Author: gao.gwq@alibaba-inc.com
 * @Date: 2021/6/10 7:18 下午
 * @Version: 1.0
 * @Description: TODO
 */
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Component
public @interface RetryTaskAnnotation {
    /**
     * projectName 和申请的项目相同
     * @return
     */
    String projectName();

    /**
     * 任务名称
     *
     * @return
     */
    String taskName();
}
