package com.retry.task.core.task.interfaces;

import com.retry.task.core.model.RetryTaskContext;
import com.retry.task.core.task.ConsumeStatus;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/4  21:56
 * @Description TODO
 */
public interface IRetryTaskListener {

    /**
     *
     * @param retryTaskContext
     * @return
     */
    ConsumeStatus consume(RetryTaskContext retryTaskContext);
}
