package com.retry.task.core.utils.threadPool;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2021/9/10  10:26 上午
 * @Description TODO
 */
public class RetryTaskRunable<T> implements Runnable {
    /**
     * traceId上下文， Object rpcContext = EagleEye.currentRpcContext();
     */
    private final Object context;

    private static ThreadLocal<Object> THREAD_LOCAL = new ThreadLocal<>();

    public static Object getContext() {
        return THREAD_LOCAL.get();
    }

    private final Runnable iCallBack;

    public RetryTaskRunable(Object context, Runnable callBack) {
        if (callBack == null) {
            throw new IllegalArgumentException("callBack can not be null");
        }
        this.context = context;
        this.iCallBack = callBack;
    }

    @Override
    public void run() {
        try {
            if (context != null) {
                THREAD_LOCAL.set(context);
            }
            iCallBack.run();
        } finally {
            THREAD_LOCAL.remove();
        }
    }

}
