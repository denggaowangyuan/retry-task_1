package com.retry.task.core.utils;

/**
 * @Author: gaoweiqiang
 * @email: gao.gwq@alibaba-inc.com
 * @Date: 2019/11/5 11:02 下午
 * @Version: 1.0
 * @Description: 回调函数
 */
public interface ICallBack {
     Object callBack(Object... params);
}
