package com.retry.task.core.task.interfaces;

import com.retry.task.core.model.RetryTaskContext;
import com.retry.task.core.task.ConsumeStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/6  16:50
 * @Description TODO
 */
public abstract class AbstractRetryTaskListener implements IRetryTaskListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRetryTaskListener.class);

    final public ConsumeStatus consumeContext(RetryTaskContext retryTaskContext) {
        try {
            beforeConsume(retryTaskContext);
        } catch (Exception ex) {
            LOGGER.error("retry-task listener beforeConsume {}", ex.getMessage(), ex);
        }

        ConsumeStatus consumeStatus = null;
        try {
            consumeStatus = consume(retryTaskContext);
            return consumeStatus;
        } finally {
            afterConsume(consumeStatus, retryTaskContext);
        }
    }

    @Override
    public ConsumeStatus consume(RetryTaskContext retryTaskContext) {
        return ConsumeStatus.SUCESS;
    }

    public void beforeConsume(RetryTaskContext context) {

    }

    public void afterConsume(ConsumeStatus consumeStatus, RetryTaskContext context) {

    }
}
