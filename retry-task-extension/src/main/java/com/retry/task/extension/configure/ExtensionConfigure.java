package com.retry.task.extension.configure;

import com.retry.task.extension.IExtensionFinder;
import com.retry.task.extension.impl.ExtensionFinderImpl;
import com.retry.task.extension.processor.ExtensionProcessor;
import com.retry.task.extension.repository.ExtensionRepositoryImpl;
import com.retry.task.extension.repository.IExtensionRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ExtensionConfigure {

    @Bean
    public IExtensionRepository iExtensionRepository() {
        return new ExtensionRepositoryImpl();
    }

    @Bean
    public ExtensionProcessor extensionProcessor() {
        return new ExtensionProcessor();
    }

    @Bean
    public IExtensionFinder iExtensionFinder() {
        return new ExtensionFinderImpl();
    }
}
