package com.retry.task.extension.model;

/**
 * 扩展点场景
 */
public class ExtensionSence<T> {


    /**
     * 实现到扩展类
     */
    private Class<T> clazz;

    /**
     * 扩展点场景码
     */
    private String sence;

    /**
     * 扩展点使用点
     */
    private String useCase;

    public static <T> ExtensionSence valueOf(String sence,String useCase) {
        ExtensionSence extensionSence = new ExtensionSence();
        extensionSence.setSence(sence);
        extensionSence.setUseCase(useCase);
        return extensionSence;
    }


    public Class<T> getClazz() {
        return clazz;
    }

    public String getSence() {
        return sence;
    }

    public String getUseCase() {
        return useCase;
    }

    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void setSence(String sence) {
        this.sence = sence;
    }

    public void setUseCase(String useCase) {
        this.useCase = useCase;
    }
}
