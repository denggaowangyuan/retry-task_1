package com.retry.task.extension.repository;

import com.retry.task.extension.IExtension;
import com.retry.task.extension.model.ExtensionSence;

import java.util.Collection;

/**
 * 扩展点加载器
 */
 public interface IExtensionRepository {


    /**
     * 项目启动点时候将对象加载到缓存中
     *
     * @param extensionSence
     * @return
     */
    void loadExtension(IExtension extension, ExtensionSence extensionSence);


    /**
     * 获取扩展点
     *
     * @param extensionSence
     * @return
     */
    IExtension getExtension(ExtensionSence extensionSence);

    /**
     * 获取场景下所有实现到扩展点
     *
     * @param extensionSence
     * @return
     */
    Collection<IExtension> getExtensionList(ExtensionSence extensionSence);
}
