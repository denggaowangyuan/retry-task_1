package com.retry.task.extension.impl;

import com.retry.task.extension.IExtensionFinder;
import com.retry.task.extension.model.ExtensionSence;
import com.retry.task.extension.repository.IExtensionRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ExtensionFinderImpl implements IExtensionFinder {


    @Autowired
    private IExtensionRepository iExtensionRepository;


    @Override
    public <T> T getExtension(ExtensionSence extensionSence, Class<T> clazz) {
        checkParams(extensionSence, clazz);
        extensionSence.setClazz(clazz);
        return (T) iExtensionRepository.getExtension(extensionSence);
    }

    @Override
    public <T> List<T> getSenceExtensionList(ExtensionSence extensionSence, Class<T> clazz) {
        checkParams(extensionSence, clazz);
        extensionSence.setClazz(clazz);
        return (List<T>) iExtensionRepository.getExtensionList(extensionSence);
    }

    private static <T> void checkParams(ExtensionSence extensionSence, Class<T> clazz) {
        String sence = extensionSence.getSence();
        if (sence == null || "".equals(sence.trim())) {
            throw new IllegalArgumentException("sence is null");
        }
        String useCase = extensionSence.getUseCase();
        if (useCase == null || "".equals(useCase.trim())) {
            throw new IllegalArgumentException("useCase is null");
        }
        if (clazz == null) {
            throw new IllegalArgumentException("clazz is null");
        }
    }
}
