package com.retry.task.extension.repository;

import com.retry.task.extension.IExtension;
import com.retry.task.extension.model.ExtensionOperation;
import com.retry.task.extension.model.ExtensionSence;
import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 扩展点管理类
 */
public class ExtensionRepositoryImpl implements IExtensionRepository {


    private static final Logger LOGGER = LoggerFactory.getLogger(ExtensionRepositoryImpl.class);
    /**
     * 扩展点管理缓存，存储扩展点唯一性key和对应的对象
     */
    private Map<String, ExtensionOperation> extensionCacheMap = new ConcurrentHashMap<>();


    /**
     *
     */
    private Map<String, List<ExtensionOperation>> sceneCacheMap = new ConcurrentHashMap<>();


    @Override
    public void loadExtension(IExtension extension, ExtensionSence extensionSence) {

        ExtensionOperation operation = buildExtensionOperation(extensionSence);
        operation.setiExtension(extension);
        String uniqueKey = operation.uniqueUseCaseKey();
        ExtensionOperation extensionOperation = extensionCacheMap.get(uniqueKey);
        if (extensionOperation != null) {
            LOGGER.warn("[ExtensionRepositoryImpl-loadExtension] duplicate extension ,key = {}", uniqueKey);
            throw new RuntimeException("duplicate extension ,key = " + uniqueKey);
        }
        extensionCacheMap.put(uniqueKey, operation);

        String senceKey = operation.uniqueSenceKey();
        List<ExtensionOperation> operationList = sceneCacheMap.get(senceKey);
        if (CollectionUtils.isEmpty(operationList)) {
            operationList = new ArrayList<>();
            sceneCacheMap.put(senceKey, operationList);
        }
        operationList.add(operation);
    }

    private ExtensionOperation buildExtensionOperation(ExtensionSence extensionSence) {
        ExtensionOperation operation = new ExtensionOperation();
        operation.setClazz(extensionSence.getClazz());
        operation.setUseCase(extensionSence.getUseCase());
        operation.setSence(extensionSence.getSence());
        return operation;
    }


    @Override
    public IExtension getExtension(ExtensionSence extensionSence) {
        ExtensionOperation operation = buildExtensionOperation(extensionSence);
        String useCaseKey = operation.uniqueUseCaseKey();
        ExtensionOperation realOperation = extensionCacheMap.get(useCaseKey);
        if (realOperation == null) {
            return null;
        }
        return realOperation.getiExtension();
    }

    @Override
    public Collection<IExtension> getExtensionList(ExtensionSence extensionSence) {
        ExtensionOperation operation = buildExtensionOperation(extensionSence);
        String senceKey = operation.uniqueSenceKey();
        List<ExtensionOperation> realOperationList = sceneCacheMap.get(senceKey);
        if (realOperationList == null) {
            return null;
        }
        List<IExtension> iExtensionList = realOperationList
                .stream().map(ExtensionOperation::getiExtension).collect(Collectors.toList());

        return iExtensionList;
    }
}
