package com.retry.task.extension.model;

import com.retry.task.extension.IExtension;

/**
 * 扩展点封装类
 */
public class ExtensionOperation {

    private String sence;


    private String useCase;

    private Class<?> clazz;

    private IExtension iExtension;


    public String getSence() {
        return sence;
    }

    public void setSence(String sence) {
        this.sence = sence;
    }

    public String getUseCase() {
        return useCase;
    }

    public void setUseCase(String useCase) {
        this.useCase = useCase;
    }

    public IExtension getiExtension() {
        return iExtension;
    }

    public void setiExtension(IExtension iExtension) {
        this.iExtension = iExtension;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public String uniqueUseCaseKey() {
        return clazz.getName() + "_" + sence + "_" + useCase;
    }

    public String uniqueSenceKey() {
        return clazz.getName() + "_" + sence;
    }
}
