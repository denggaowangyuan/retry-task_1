package com.retry.task.extension;


import com.retry.task.extension.model.ExtensionSence;

import java.util.List;

/**
 *
 */
public interface IExtensionFinder {


    /**
     * 获取扩展点
     *
     * @param extensionSence
     * @return
     */
    <T> T getExtension(ExtensionSence extensionSence, Class<T> clazz);

    /**
     * 获取场景实现类
     *
     * @param extensionSence
     * @return
     */
    <T> List<T> getSenceExtensionList(ExtensionSence extensionSence,Class<T> clazz);
}
