package com.retry.task.extension.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 扩展点注释类
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface ExtensionAnnotation {


    /**
     * 实现到扩展类
     *
     * @return
     */
    Class<?>[] extensionClass();

    /**
     * 使用场景，一个场景下会有多个case，不同的场景使用不同的描述
     *
     * @return
     */
    String scene();


    /**
     * 场景下具体的使用点
     *
     * @return
     */
    String useCase();


}
