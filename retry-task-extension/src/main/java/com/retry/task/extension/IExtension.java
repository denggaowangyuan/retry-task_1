package com.retry.task.extension;

/**
 * 扩展点的空方法，所有扩展点接口都需要实现该方法。
 *
 */
public interface IExtension {
}
