package com.retry.task.admin.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/27  13:12
 * @Description TODO
 */
public class RetryTaskGroupDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = -1222297496076604728L;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 机器ip
     */
    private String executeIp;

    /**
     * 机器类型
     */
    private Integer executeType;

    /**
     * 心跳时间
     */
    private Date heartTime;

    /**
     * 心跳次数
     */
    private Long heartTick;

    /**
     * 活动级别
     */
    private Integer activeLevel;

    private Integer status ;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getExecuteIp() {
        return executeIp;
    }

    public void setExecuteIp(String executeIp) {
        this.executeIp = executeIp;
    }

    public Integer getExecuteType() {
        return executeType;
    }

    public void setExecuteType(Integer executeType) {
        this.executeType = executeType;
    }

    public Date getHeartTime() {
        return heartTime;
    }

    public void setHeartTime(Date heartTime) {
        this.heartTime = heartTime;
    }

    public Long getHeartTick() {
        return heartTick;
    }

    public void setHeartTick(Long heartTick) {
        this.heartTick = heartTick;
    }

    public Integer getActiveLevel() {
        return activeLevel;
    }

    public void setActiveLevel(Integer activeLevel) {
        this.activeLevel = activeLevel;
    }
}
