package com.retry.task.admin.dal.mapper;

import com.retry.task.admin.dal.model.RetryTaskGroupDO;
import com.retry.task.datasource.annotation.MultiMapper;

import java.util.List;
import java.util.Map;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/21  14:49
 * @Description TODO
 */
@MultiMapper(sqlSessionFactoryBeanName = "retryTaskSqlSessionFactory")
public interface RetryTaskGroupMapper extends BaseMapper<RetryTaskGroupDO, Long> {

    List<RetryTaskGroupDO> queryByCondition(Map<String, Object> params);

    int countByCondition(Map<String, Object> params);

    int updateByCondition(Map<String, Object> params);


    int updateHeartTickNumById(Long id);
}
