package com.retry.task.admin.dal.mapper;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2021/9/6  2:31 下午
 * @Description TODO
 */
public interface BaseMapper<T, PK extends Serializable> {
    /**
     * 按主键取记录
     *
     * @return 记录实体对象，如果没有符合主键条件的记录，则返回null
     */
    T queryByPrimaryKey(PK id);


    int insert(T object);

    /**
     * 批量插入
     *
     * @param list
     * @return 返回主键，分库分表时返回null，如果出错则throw FulfillDALException
     */
    int batchInsert(Collection<T> list);

    /**
     * 更新主键修改
     *
     * @param object
     * @return
     */
    int updateByPrimaryKey(T object);

    /**
     * 根据主键删除
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);
}
