package com.retry.task.admin.infrastructure.selector;

import java.util.HashMap;
import java.util.Map;

import com.retry.task.admin.infrastructure.selector.impl.LatestHeartSelector;
import com.retry.task.admin.infrastructure.selector.impl.PollingSelector;
import com.retry.task.admin.infrastructure.selector.impl.RandomSelector;
import com.retry.task.core.constants.SelectorExectorEnum;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/12  13:35
 * @Description TODO
 */
public class SelectorManager {

    private static final Map<String, ISelectExector> EXECTOR_MAP = new HashMap<String, ISelectExector>() {{
        put(SelectorExectorEnum.LAST_HEART.name(), new LatestHeartSelector());
        put(SelectorExectorEnum.RANDOM.name(), new RandomSelector());
        put(SelectorExectorEnum.POLLING.name(), new PollingSelector());
    }};

    private SelectorManager() {}

    public static ISelectExector getSelector(String key) {
        return EXECTOR_MAP.get(key);
    }
}
