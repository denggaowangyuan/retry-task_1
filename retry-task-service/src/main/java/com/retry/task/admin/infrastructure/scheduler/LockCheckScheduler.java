package com.retry.task.admin.infrastructure.scheduler;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/13  16:38
 * @Description TODO 为了防止出现锁不被释放的情况，对使用超过一个小时的锁，进行强制释放
 */
public class LockCheckScheduler extends BaseScheduler{
    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
