package com.retry.task.admin.infrastructure.warn;

import com.retry.task.admin.infrastructure.model.WarnModel;
import com.retry.task.core.utils.GsonTool;
import com.retry.task.extension.IExtensionFinder;
import com.retry.task.extension.model.ExtensionSence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/9/24  14:05
 * @Description TODO
 */
@Component
public class WarnDomainService {

    private static Logger LOGGER = LoggerFactory.getLogger(WarnDomainService.class);

    @Autowired
    private IExtensionFinder iExtensionFinder;

    /**
     * 异常告警
     *
     * @param warnModelList
     * @return
     */
    public boolean failTaskWarn(List<WarnModel> warnModelList) {
        if (CollectionUtils.isEmpty(warnModelList)) {
            return false;
        }
        boolean warnFlag = true;
        for (WarnModel warnModel : warnModelList) {
            IWarn warn = iExtensionFinder.getExtension(ExtensionSence.valueOf("warn", warnModel.getWarnType()), IWarn.class);
            if (warn == null) {
                continue;
            }
            boolean realWarnFlag = warn.asyncWarn(warnModel);
            if (realWarnFlag == false) {
                warnFlag = false;
                LOGGER.warn("WarnDomainService-failTaskWarn,warn fail {}", GsonTool.toJsonString(warnModel));
            }
        }
        return warnFlag;
    }
}