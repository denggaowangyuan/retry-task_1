package com.retry.task.admin.dal.mapper;

import com.retry.task.admin.dal.model.UserInfoDO;
import com.retry.task.datasource.annotation.MultiMapper;

import java.util.List;
import java.util.Map;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/11  22:01
 * @Description TODO
 */
@MultiMapper(sqlSessionFactoryBeanName = "retryTaskSqlSessionFactory")
public interface UserInfoMapper extends BaseMapper<UserInfoDO, Long> {

    List<UserInfoDO> queryByCondition(Map<String, Object> params);

    int countByCondition(Map<String, Object> params);

    int updateByCondition(Map<String, Object> params);
}
