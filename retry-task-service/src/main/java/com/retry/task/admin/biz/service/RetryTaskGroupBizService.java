package com.retry.task.admin.biz.service;

import com.google.common.collect.Lists;
import com.retry.task.admin.dal.mapper.RetryTaskGroupMapper;
import com.retry.task.admin.dal.model.RetryTaskGroupDO;
import com.retry.task.admin.dal.model.query.RetryTaskGroupQuery;
import com.retry.task.core.model.HeartDTO;
import com.retry.task.core.model.base.PageResult;
import com.retry.task.core.model.base.Result;
import com.retry.task.core.utils.CommonExecuteUtils;
import com.retry.task.sequence.Sequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/30  00:21
 * @Description TODO
 */
@Service
public class RetryTaskGroupBizService {
    @Autowired
    private RetryTaskGroupMapper retryTaskGroupMapper;

    @Autowired
    private Sequence retryTaskGroupSequence;

    public List<RetryTaskGroupDO> queryByRetryTaskGroups(RetryTaskGroupQuery query) {
        Map<String, Object> paramMap = CommonExecuteUtils.beanToMap(query, false);
        paramMap.put("offset", query.getOffset());
        paramMap.put("limit", query.getPageSize());
        List<RetryTaskGroupDO> retryTaskGroupDOList = retryTaskGroupMapper.queryByCondition(paramMap);
        return retryTaskGroupDOList;
    }

    public PageResult<List<RetryTaskGroupDO>> queryByRetryTaskGroupByPage(RetryTaskGroupQuery query) {
        Map<String, Object> paramMap = CommonExecuteUtils.beanToMap(query, false);
        paramMap.put("offset", query.getOffset());
        paramMap.put("limit", query.getPageSize());
        PageResult<List<RetryTaskGroupDO>> pageResult = PageResult.getSuccess(null);
        int count = retryTaskGroupMapper.countByCondition(paramMap);
        pageResult.setTotalCount(count);
        if (count == 0) {
            return pageResult;
        }
        List<RetryTaskGroupDO> retryTaskGroupDOList = retryTaskGroupMapper.queryByCondition(paramMap);
        pageResult.setData(retryTaskGroupDOList);
        return pageResult;
    }

    public int updateRetryTaskGroupById(Long id, RetryTaskGroupDO retryTaskGroupDO) {
        if (id == null) {
            return -1;
        }
        if (retryTaskGroupDO == null) {
            return -1;
        }

        retryTaskGroupDO.setId(id);
        return retryTaskGroupMapper.updateByPrimaryKey(retryTaskGroupDO);
    }

    @Transactional
    public Result<RetryTaskGroupDO> heartTick(HeartDTO heartDTO) {

        RetryTaskGroupDO taskGroupDO = new RetryTaskGroupDO();
        taskGroupDO.setExecuteType(heartDTO.getExecuteType());
        taskGroupDO.setProjectName(heartDTO.getProjectName());
        taskGroupDO.setExecuteIp(heartDTO.getClientIp());
        taskGroupDO.setIsTest(heartDTO.getIsTest());
        Map<String, Object> paramMap = CommonExecuteUtils.beanToMap(taskGroupDO, false);
        paramMap.put("limit", 100);
        paramMap.put("offset", 0);
        List<RetryTaskGroupDO> retryTaskGroupDOList = retryTaskGroupMapper.queryByCondition(paramMap);
        if (CollectionUtils.isEmpty(retryTaskGroupDOList)) {
            taskGroupDO.setHeartTick(1L);
            taskGroupDO.setHeartTime(new Date());
            taskGroupDO.setId(retryTaskGroupSequence.nextValue());
            taskGroupDO.setIsTest(heartDTO.getIsTest());
            taskGroupDO.setStatus(1);
            taskGroupDO.setHeartTick(0L);
            retryTaskGroupMapper.batchInsert(Lists.newArrayList(taskGroupDO));
            return Result.getSuccess(taskGroupDO);
        }
        RetryTaskGroupDO oldRetryTaskGroup = retryTaskGroupDOList.get(0);
        retryTaskGroupMapper.updateHeartTickNumById(oldRetryTaskGroup.getId());
        /*oldRetryTaskGroup.setHeartTime(new Date());
        oldRetryTaskGroup.setStatus(1);
        oldRetryTaskGroup.setHeartTick(oldRetryTaskGroup.getHeartTick() + 1);
        retryTaskGroupMapper.updateByPrimaryKey(oldRetryTaskGroup);*/
        return Result.getSuccess(oldRetryTaskGroup);
    }
}
