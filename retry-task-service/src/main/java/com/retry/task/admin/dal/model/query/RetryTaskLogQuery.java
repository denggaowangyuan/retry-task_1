package com.retry.task.admin.dal.model.query;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;


/**
 * @author gao.gwq
 * @version 1.0
 * @date 2021/11/11  6:03 下午
 * @Description TODO
 */
public class RetryTaskLogQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = -816172228566779938L;

    private String projectName;

    private String taskName;

    private Integer status;

    private Long taskId;

    private Collection<Long> taskIds;

    public Collection<Long> getTaskIds() {
        return taskIds;
    }

    public void setTaskIds(Collection<Long> taskIds) {
        this.taskIds = taskIds;
    }
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date minPreStartTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date maxPreStartTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date minStartTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date maxStartTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date minEndTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date maxEndTime;

    private String executeIp;

    private Collection<String> executeIps;

    public Collection<String> getExecuteIps() {
        return executeIps;
    }

    public void setExecuteIps(Collection<String> executeIps) {
        this.executeIps = executeIps;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Date getMinPreStartTime() {
        return minPreStartTime;
    }

    public void setMinPreStartTime(Date minPreStartTime) {
        this.minPreStartTime = minPreStartTime;
    }

    public Date getMaxPreStartTime() {
        return maxPreStartTime;
    }

    public void setMaxPreStartTime(Date maxPreStartTime) {
        this.maxPreStartTime = maxPreStartTime;
    }

    public Date getMinStartTime() {
        return minStartTime;
    }

    public void setMinStartTime(Date minStartTime) {
        this.minStartTime = minStartTime;
    }

    public Date getMaxStartTime() {
        return maxStartTime;
    }

    public void setMaxStartTime(Date maxStartTime) {
        this.maxStartTime = maxStartTime;
    }

    public Date getMinEndTime() {
        return minEndTime;
    }

    public void setMinEndTime(Date minEndTime) {
        this.minEndTime = minEndTime;
    }

    public Date getMaxEndTime() {
        return maxEndTime;
    }

    public void setMaxEndTime(Date maxEndTime) {
        this.maxEndTime = maxEndTime;
    }

    public String getExecuteIp() {
        return executeIp;
    }

    public void setExecuteIp(String executeIp) {
        this.executeIp = executeIp;
    }
}
