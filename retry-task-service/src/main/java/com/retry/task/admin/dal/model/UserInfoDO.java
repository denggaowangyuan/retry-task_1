package com.retry.task.admin.dal.model;

import java.io.Serializable;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/27  13:12
 * @Description TODO
 */
public class UserInfoDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = -675787432674211680L;
    /**
     * 项目名称
     */
    private String userName;

    private String password;

    private String attribute;
    private String projectId;

    private Integer role;

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }
}
