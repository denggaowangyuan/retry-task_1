package com.retry.task.admin.base.sequence;

import com.retry.task.sequence.Sequence;
import com.retry.task.sequence.SequenceDao;
import com.retry.task.sequence.impl.DefaultSequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/28  15:24
 * @Description TODO
 */
@Configuration
public class SquenceService {

    @Autowired
    private DataSource retryTaskDataSource;

    @Bean
    public SequenceDao defaultSequenceDao() {

        DefaultSequenceDaoBuilder sequenceDaoBuilder = DefaultSequenceDaoBuilder.create();
        sequenceDaoBuilder.dataSource(retryTaskDataSource);
        return sequenceDaoBuilder.build();
    }

    @Bean
    public Sequence retryTaskSequence(@Autowired  SequenceDao defaultSequenceDao) {
        DefaultSequence defaultSequence = new DefaultSequence();
        defaultSequence.setSequenceDao(defaultSequenceDao);
        defaultSequence.setName("retry_task");
        return defaultSequence;
    }

    @Bean
    public Sequence retryTaskLogSequence(@Autowired  SequenceDao defaultSequenceDao) {
        DefaultSequence defaultSequence = new DefaultSequence();
        defaultSequence.setSequenceDao(defaultSequenceDao);
        defaultSequence.setName("retry_task_log");
        return defaultSequence;
    }

    @Bean
    public Sequence retryTaskGroupSequence(@Autowired  SequenceDao defaultSequenceDao) {
        DefaultSequence defaultSequence = new DefaultSequence();
        defaultSequence.setSequenceDao(defaultSequenceDao);
        defaultSequence.setName("retry_task_group");
        return defaultSequence;
    }

    @Bean
    public Sequence retryTaskLockSequence(@Autowired  SequenceDao defaultSequenceDao) {
        DefaultSequence defaultSequence = new DefaultSequence();
        defaultSequence.setSequenceDao(defaultSequenceDao);
        defaultSequence.setName("retry_task_lock");
        return defaultSequence;
    }

    @Bean
    public Sequence retryTaskProjectSequence(@Autowired  SequenceDao defaultSequenceDao) {
        DefaultSequence defaultSequence = new DefaultSequence();
        defaultSequence.setSequenceDao(defaultSequenceDao);
        defaultSequence.setName("retry_task_project");
        return defaultSequence;
    }

    @Bean
    public Sequence userInfoSequence(@Autowired  SequenceDao defaultSequenceDao) {
        DefaultSequence defaultSequence = new DefaultSequence();
        defaultSequence.setSequenceDao(defaultSequenceDao);
        defaultSequence.setName("user_info");
        return defaultSequence;
    }
}
