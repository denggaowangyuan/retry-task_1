package com.retry.task.admin.dal.mapper;

import com.retry.task.admin.dal.model.RetryTaskLogDO;
import com.retry.task.datasource.annotation.MultiMapper;

import java.util.List;
import java.util.Map;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/21  14:48
 * @Description TODO
 */
@MultiMapper(sqlSessionFactoryBeanName = "retryTaskSqlSessionFactory")
public interface RetryTaskLogMapper extends BaseMapper<RetryTaskLogDO,Long> {

    List<RetryTaskLogDO> queryByCondition(Map<String, Object> params);

    int countByCondition(Map<String, Object> params);

    int updateByCondition(Map<String, Object> params);
}

