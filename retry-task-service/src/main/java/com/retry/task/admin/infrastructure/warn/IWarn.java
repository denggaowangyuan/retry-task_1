package com.retry.task.admin.infrastructure.warn;

import com.retry.task.admin.infrastructure.model.WarnModel;
import com.retry.task.extension.IExtension;

import java.util.List;

/**
 *
 */
public interface IWarn extends IExtension {


    /**
     * 异步通知
     * @param warnModel
     * @return
     */
    boolean asyncWarn(WarnModel warnModel);


    /**
     * 同步通知
     * @param warnModel
     * @return
     */
    boolean syncWarn(WarnModel warnModel);
}
