package com.retry.task.admin.infrastructure.selector.impl;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.retry.task.admin.dal.model.RetryTaskGroupDO;
import com.retry.task.admin.infrastructure.selector.ISelectExector;
import com.retry.task.extension.annotation.ExtensionAnnotation;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/12  13:29
 * @Description TODO 轮询
 */
@ExtensionAnnotation(extensionClass = ISelectExector.class,scene = "selectExector",useCase = "PollingSelector")
public class PollingSelector implements ISelectExector {

    private static ThreadLocal<Integer> SELECTOR_NUM = new ThreadLocal<Integer>() {
        @Override
        protected Integer initialValue() {
            return 0;
        }
    };

    private static final AtomicInteger NUM = new AtomicInteger(0);

    @Override
    public RetryTaskGroupDO bestExectorSelect(List<RetryTaskGroupDO> retryTaskGroupList) {
        int num = NUM.get();
        if (num > retryTaskGroupList.size()) {
            num = 0;
        }
        NUM.addAndGet(1);
        return retryTaskGroupList.get(num);
    }
}
