package com.retry.task.admin.biz.service;

import com.google.common.collect.Lists;
import com.retry.task.admin.dal.mapper.RetryTaskProjectMapper;
import com.retry.task.admin.dal.model.RetryTaskProjectDO;
import com.retry.task.admin.dal.model.query.RetryTaskProjectQuery;
import com.retry.task.core.model.base.PageResult;
import com.retry.task.core.model.base.Result;
import com.retry.task.core.utils.CommonExecuteUtils;
import com.retry.task.sequence.Sequence;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/1  15:30
 * @Description TODO
 */
@Service
public class RetryTaskProjectBizService {

    @Autowired
    private RetryTaskProjectMapper retryTaskProjectMapper;

    @Autowired
    private Sequence retryTaskProjectSequence;

    public Result<String> getRetryTaskProjectToken(String projectName, Integer isTest) {
        RetryTaskProjectQuery projectQuery = new RetryTaskProjectQuery();
        projectQuery.setProjectName(projectName);
        projectQuery.setIsTest(isTest);
        PageResult<List<RetryTaskProjectDO>> resultDTO = getRetryTaskProject(projectQuery);
        if (!resultDTO.isSuccess()) {
            return Result.getFail("100", resultDTO.getErrMsg());
        }
        List<RetryTaskProjectDO> retryTaskProjectDOList = resultDTO.getData();
        if (CollectionUtils.isEmpty(retryTaskProjectDOList)) {
            return Result.getFail("100", "project is not register");
        }
        return Result.getSuccess(retryTaskProjectDOList.get(0).getToken());
    }

    public Result<RetryTaskProjectDO> insertRetryTaskProject(RetryTaskProjectDO retryTaskProjectDO) {
        if (retryTaskProjectDO.getIsTest() == null) {
            return Result.getFail("100", "isTest is null");
        }
        if (retryTaskProjectDO.getProjectName() == null) {
            return Result.getFail("100", "projectName is null");
        }
        if (StringUtils.isEmpty(retryTaskProjectDO.getToken())) {
            return Result.getFail("100", "token is null");
        }

        retryTaskProjectDO.setId(retryTaskProjectSequence.nextValue());
        retryTaskProjectMapper.batchInsert(Lists.newArrayList(retryTaskProjectDO));
        return Result.getSuccess(retryTaskProjectDO);
    }

    public Result<Integer> deleteByRetryTaskProjectId(Long id) {
        if (id == null) {
            return Result.getFail("100", "id is null");
        }
        int num = retryTaskProjectMapper.deleteByPrimaryKey(id);

        return Result.getSuccess(num);
    }

    public PageResult<List<RetryTaskProjectDO>> getRetryTaskProject(RetryTaskProjectQuery retryTaskProjectQuery) {
        if (retryTaskProjectQuery.getIsTest() == null) {
            return PageResult.getFail("100", "isTest is null");
        }
        Map<String, Object> params = CommonExecuteUtils.beanToMap(retryTaskProjectQuery, false);
        Integer pageSize = retryTaskProjectQuery.getPageSize();
        if (pageSize != null && pageSize > 100) {
            return PageResult.getFail("100", "pageSize is bigger than 100");
        }
        params.put("offset", retryTaskProjectQuery.getOffset());
        params.put("limit", pageSize);
        int count = retryTaskProjectMapper.countByCondition(params);
        PageResult pageResultDTO = PageResult.getSuccess(null);
        if (count == 0) {
            return pageResultDTO;
        }
        List<RetryTaskProjectDO> retryTaskProjectDOList = retryTaskProjectMapper.queryByCondition(params);
        pageResultDTO.setData(retryTaskProjectDOList);
        pageResultDTO.setTotalCount(count);
        return pageResultDTO;
    }

    public Result<Integer> updateByRetryTaskProjectId(RetryTaskProjectDO retryTaskProjectDO) {
        if (retryTaskProjectDO.getId() == null) {
            return Result.getFail("100", "token is null");
        }
        Integer nums = retryTaskProjectMapper.updateByPrimaryKey(retryTaskProjectDO);

        return Result.getSuccess(nums);
    }

}
