package com.retry.task.admin.constants;

import java.util.Map;

import com.retry.task.admin.utils.AesUtils;
import com.retry.task.admin.utils.RSACoder;
import com.retry.task.core.exception.RetryTaskRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/13  17:21
 * @Description TODO
 */
public class Constants {

    private static final Logger LOGGER = LoggerFactory.getLogger(Constants.class);

    public static final String CODE_KEY = "1234567887654321";

    /*public static String AES_KEY = null;
    public static final Map keyMap;
    public static final byte[] publicKey;

    static {
        try {
            keyMap = RSACoder.initKey();
            publicKey = RSACoder.getPublicKey(keyMap);
            //AES_KEY = AesUtils.initKeyString();
        } catch (Exception ex) {
            LOGGER.error("init key error {}", ex.getMessage(), ex);
            throw new RetryTaskRuntimeException(ex);
        }

    }*/
}
