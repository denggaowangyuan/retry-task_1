package com.retry.task.admin.dal.model.query;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2021/11/11  6:00 下午
 * @Description TODO
 */
public class RetryTaskQuery extends BaseQuery implements Serializable {

    private static final long serialVersionUID = 5632698201339294203L;
    private List<Long> idList;
    private String solutionKey;

    private String projectName;

    private String taskName;

    private Integer status;

    private String creatorId;

    private String creatorName;

    private Integer retryNum;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date minNextPlanTime;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date maxNextPlanTime;

    private Integer minRetryNum;

    private Integer maxRetryNum;

    private Integer originRetryNum;

    private Integer minOriginRetryNum;
    private Integer maxOriginRetryNum;


    public Integer getOriginRetryNum() {
        return originRetryNum;
    }

    public void setOriginRetryNum(Integer originRetryNum) {
        this.originRetryNum = originRetryNum;
    }

    public Integer getMinOriginRetryNum() {
        return minOriginRetryNum;
    }

    public void setMinOriginRetryNum(Integer minOriginRetryNum) {
        this.minOriginRetryNum = minOriginRetryNum;
    }

    public Integer getMaxOriginRetryNum() {
        return maxOriginRetryNum;
    }

    public void setMaxOriginRetryNum(Integer maxOriginRetryNum) {
        this.maxOriginRetryNum = maxOriginRetryNum;
    }

    public Integer getMinRetryNum() {
        return minRetryNum;
    }

    public List<Long> getIdList() {
        return idList;
    }

    public void setIdList(List<Long> idList) {
        this.idList = idList;
    }

    public void setMinRetryNum(Integer minRetryNum) {
        this.minRetryNum = minRetryNum;
    }

    public Integer getMaxRetryNum() {
        return maxRetryNum;
    }

    public void setMaxRetryNum(Integer maxRetryNum) {
        this.maxRetryNum = maxRetryNum;
    }

    public Integer getRetryNum() {
        return retryNum;
    }

    public void setRetryNum(Integer retryNum) {
        this.retryNum = retryNum;
    }

    public Date getMinNextPlanTime() {
        return minNextPlanTime;
    }

    public void setMinNextPlanTime(Date minNextPlanTime) {
        this.minNextPlanTime = minNextPlanTime;
    }

    public Date getMaxNextPlanTime() {
        return maxNextPlanTime;
    }

    public void setMaxNextPlanTime(Date maxNextPlanTime) {
        this.maxNextPlanTime = maxNextPlanTime;
    }

    public String getSolutionKey() {
        return solutionKey;
    }

    public void setSolutionKey(String solutionKey) {
        this.solutionKey = solutionKey;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }
}
