package com.retry.task.admin.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/27  13:13
 * @Description TODO
 */
public class BaseDO implements Serializable {
    private static final long serialVersionUID = 9077905730400069121L;

    /**
     * ID
     *
     * This field corresponds to the database column retry_task_log.id
     */
    private Long id;

    /**
     * 创建时间
     *
     * This field corresponds to the database column retry_task_log.gmt_create
     */
    private Date gmtCreate;

    /**
     * 修改时间
     *
     * This field corresponds to the database column retry_task_log.gmt_modified
     */
    private Date gmtModified;

    /**
     * 是否测试 1 测试环境 0 线上环境
     *
     * This field corresponds to the database column retry_task_log.is_test
     */
    private Integer isTest;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getIsTest() {
        return isTest;
    }

    public void setIsTest(Integer isTest) {
        this.isTest = isTest;
    }
}
