package com.retry.task.admin.infrastructure.selector.impl;

import java.util.List;
import java.util.Random;

import com.retry.task.admin.dal.model.RetryTaskGroupDO;
import com.retry.task.admin.infrastructure.selector.ISelectExector;
import com.retry.task.extension.annotation.ExtensionAnnotation;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/12  13:16
 * @Description TODO 随机获取一个可以执行执行client
 */
@ExtensionAnnotation(extensionClass = ISelectExector.class, scene = "selectExector", useCase = "RandomSelector")
public class RandomSelector implements ISelectExector {

    private Random random = new Random();

    @Override
    public RetryTaskGroupDO bestExectorSelect(List<RetryTaskGroupDO> retryTaskGroupList) {

        int size = retryTaskGroupList.size();

        int randomVal = random.nextInt(size);
        return retryTaskGroupList.get(randomVal);
    }

    public static void main(String[] args) {
        Random random = new Random();
        for (int i = 0; i < 100; i++) {

            int randomVal = random.nextInt(5);
            System.out.println(randomVal);
        }

    }
}
