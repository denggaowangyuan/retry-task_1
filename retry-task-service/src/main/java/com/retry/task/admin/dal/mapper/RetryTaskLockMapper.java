package com.retry.task.admin.dal.mapper;

import com.retry.task.admin.dal.model.RetryTaskLockDO;
import com.retry.task.datasource.annotation.MultiMapper;

import java.util.List;
import java.util.Map;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/21  14:49
 * @Description TODO
 */
@MultiMapper(sqlSessionFactoryBeanName = "retryTaskSqlSessionFactory")
public interface RetryTaskLockMapper extends BaseMapper<RetryTaskLockDO, Long> {

    List<RetryTaskLockDO> queryByCondition(Map<String, Object> params);

    int countByCondition(Map<String, Object> params);

    int updateByCondition(Map<String, Object> params);

    RetryTaskLockDO getProjectLock(Map<String, Object> params);

    int releaseLock(Map<String, Object> params);
}
