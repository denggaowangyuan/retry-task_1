package com.retry.task.admin.infrastructure.selector;

import java.util.List;

import com.retry.task.admin.dal.model.RetryTaskGroupDO;
import com.retry.task.extension.IExtension;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/7  00:13
 * @Description TODO 选择执行器的方式
 * hash 轮询 随机
 */
public interface ISelectExector extends IExtension {

    /**
     * 获取最优的执行器
     * @param retryTaskGroupList
     * @return
     */
    RetryTaskGroupDO bestExectorSelect(List<RetryTaskGroupDO> retryTaskGroupList);
}
