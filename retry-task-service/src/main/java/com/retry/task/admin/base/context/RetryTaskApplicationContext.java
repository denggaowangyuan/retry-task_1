package com.retry.task.admin.base.context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2021/9/8  1:18 下午
 * @Description TODO
 */
@Component
public class RetryTaskApplicationContext implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        setContext(context);
    }

    private static void setContext(ApplicationContext context) {
        applicationContext = context;
    }


    public static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }

    public static <T> T getBeanByName(String name) {
        return (T) applicationContext.getBean(name);
    }

}
