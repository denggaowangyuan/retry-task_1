package com.retry.task.admin.constants;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/12  21:39
 * @Description TODO
 */
public class SystemLocks {

    public static final String FAIL_TASK_CHECK_LOCK = "fail_task_check";

    public static final String RETRY_TASK_PUBLISH_LOCK = "retry_task_publish";

    //清理五分钟没有心跳的
    public static final String NO_HEART_TICK_OVER_5_MIN="clear_no_heart";
}
