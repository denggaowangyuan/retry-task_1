package com.retry.task.admin.utils;

import java.util.Set;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/12  23:48
 * @Description TODO
 */
public class FilterUtils {
    public static boolean isInWhiteList(Set<String> whiteList, String requestURI) {

        for (String whiteURI : whiteList) {
            if (isMatch(requestURI, whiteURI)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMatch(String str, String pattern) {
        int i = 0;
        int j = 0;
        int starIndex = -1;
        int iIndex = -1;

        while (i < str.length()) {
            if (j < pattern.length() && (pattern.charAt(j) == '?' || pattern.charAt(j) == str.charAt(i))) {
                ++i;
                ++j;
            } else if (j < pattern.length() && pattern.charAt(j) == '*') {
                starIndex = j;
                iIndex = i;
                j++;
            } else if (starIndex != -1) {
                j = starIndex + 1;
                i = iIndex + 1;
                iIndex++;
            } else {
                return false;
            }
        }

        while (j < pattern.length() && pattern.charAt(j) == '*') {
            ++j;
        }

        return j == pattern.length();
    }
}
