package com.retry.task.admin.dal.model;

import java.util.Date;

/**
 * This class corresponds to the database table retry_task_log
 *
 * @author gao.gwq
 * @date 2021/11/12
 */
public class RetryTaskLogDO extends BaseDO{


    /**
     * 所引用的项目名称
     *
     * This field corresponds to the database column retry_task_log.project_name
     */
    private String projectName;

    /**
     * 需要执行的任务名称
     *
     * This field corresponds to the database column retry_task_log.task_name
     */
    private String taskName;

    /**
     * 任务描述
     *
     * This field corresponds to the database column retry_task_log.task_id
     */
    private Long taskId;

    /**
     * 最终执行状态 1 执行中,-1:执行失败,2:执行成功
     *
     * This field corresponds to the database column retry_task_log.status
     */
    private Integer status;

    /**
     * 本身预计执行时间
     *
     * This field corresponds to the database column retry_task_log.pre_start_time
     */
    private Date preStartTime;

    /**
     * 执行开始时间
     *
     * This field corresponds to the database column retry_task_log.start_time
     */
    private Date startTime;

    /**
     * 执行结束时间
     *
     * This field corresponds to the database column retry_task_log.end_time
     */
    private Date endTime;

    /**
     * 执行机器ip
     *
     * This field corresponds to the database column retry_task_log.execute_ip
     */
    private String executeIp;

    /**
     * 执行轨迹id
     *
     * This field corresponds to the database column retry_task_log.trace_id
     */
    private String traceId;

    private String msgId;

    private Integer maxExecuteTime;

    public Integer getMaxExecuteTime() {
        return maxExecuteTime;
    }

    public void setMaxExecuteTime(Integer maxExecuteTime) {
        this.maxExecuteTime = maxExecuteTime;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    /**
     * 错误原因
     *
     * This field corresponds to the database column retry_task_log.error_message
     */
    private String errorMessage;



    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName == null ? null : projectName.trim();
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName == null ? null : taskName.trim();
    }

    public Date getPreStartTime() {
        return preStartTime;
    }

    public void setPreStartTime(Date preStartTime) {
        this.preStartTime = preStartTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getExecuteIp() {
        return executeIp;
    }

    public void setExecuteIp(String executeIp) {
        this.executeIp = executeIp == null ? null : executeIp.trim();
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId == null ? null : traceId.trim();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage == null ? null : errorMessage.trim();
    }

    /**
     * This method corresponds to the database table retry_task_log
     *
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", projectName=").append(projectName);
        sb.append(", taskName=").append(taskName);
        sb.append(", taskId=").append(taskId);
        sb.append(", status=").append(status);
        sb.append(", preStartTime=").append(preStartTime);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", executeIp=").append(executeIp);
        sb.append(", traceId=").append(traceId);
        sb.append(", errorMessage=").append(errorMessage);
        sb.append("]");
        return sb.toString();
    }
}