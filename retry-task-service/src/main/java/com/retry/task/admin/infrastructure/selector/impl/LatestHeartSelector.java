package com.retry.task.admin.infrastructure.selector.impl;

import java.util.List;

import com.retry.task.admin.dal.model.RetryTaskGroupDO;
import com.retry.task.admin.infrastructure.selector.ISelectExector;
import com.retry.task.extension.annotation.ExtensionAnnotation;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/12  13:11
 * @Description TODO 取最近更新心跳的机器
 */
@ExtensionAnnotation(extensionClass = ISelectExector.class,scene = "selectExector",useCase = "LatestHeartSelector")
public class LatestHeartSelector implements ISelectExector {
    @Override
    public RetryTaskGroupDO bestExectorSelect(List<RetryTaskGroupDO> retryTaskGroupList) {
        retryTaskGroupList.sort((o1, o2) -> o2.getHeartTime().compareTo(o1.getHeartTime()));
        return retryTaskGroupList.get(0);
    }
}
