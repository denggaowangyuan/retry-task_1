package com.retry.task.admin.infrastructure.warn.impl;

import com.retry.task.admin.infrastructure.model.WarnModel;
import com.retry.task.admin.infrastructure.warn.AbstarctWarn;
import com.retry.task.admin.infrastructure.warn.IWarn;
import com.retry.task.core.constants.WarnConstants;
import com.retry.task.extension.annotation.ExtensionAnnotation;

import java.util.List;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/9/24  14:01
 * @Description TODO
 */
@ExtensionAnnotation(extensionClass = IWarn.class,scene = WarnConstants.WARN,useCase = WarnConstants.DING_DING)
public class DingDingWarn extends AbstarctWarn {


    @Override
    public boolean syncWarn(WarnModel warnModel) {
        return doSendWarn(warnModel);
    }

    @Override
    public boolean asyncWarn(WarnModel warnModel) {

        sendWarn(warnModel);
        return true;
    }

    @Override
    public boolean doSendWarn(WarnModel warnModel) {
        return false;
    }
}