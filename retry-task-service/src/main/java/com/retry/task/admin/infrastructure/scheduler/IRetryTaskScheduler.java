package com.retry.task.admin.infrastructure.scheduler;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/19  21:09
 * @Description TODO
 */
public interface IRetryTaskScheduler {

    /**
     * 关闭定时任务
     */
    void stopScheduler();

    /**
     * 重启定时任务
     *
     * @return
     */
    boolean reStartScheduler();

    /**
     * 启动定时任务
     *
     * @return
     */
    boolean startScheduler();

    /**
     * 挂起，最少挂起十秒钟
     *
     * @return
     */
    boolean suspendScheduler(Long seconds);

}
