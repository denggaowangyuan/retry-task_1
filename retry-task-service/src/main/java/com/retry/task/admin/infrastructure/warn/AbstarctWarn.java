package com.retry.task.admin.infrastructure.warn;

import com.retry.task.admin.infrastructure.model.WarnModel;
import com.retry.task.core.utils.threadPool.RetryTaskThreadPoolExector;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/9/24  14:08
 * @Description TODO
 */
public abstract class AbstarctWarn implements IWarn {

    private static final ArrayBlockingQueue<Runnable> BLACKING_QUEUE = new ArrayBlockingQueue<Runnable>(300);
    public static final RetryTaskThreadPoolExector WARN_THREAD_POLL = new RetryTaskThreadPoolExector(2, 32, 60,
            TimeUnit.SECONDS, BLACKING_QUEUE, new BasicThreadFactory.Builder().namingPattern("warn-thread-poll").build());


    protected  void sendWarn(WarnModel warnModel){
        WARN_THREAD_POLL.execute(new Runnable() {
            @Override
            public void run() {
                doSendWarn(warnModel);
            }
        });
    }

    public abstract boolean  doSendWarn(WarnModel warnModel);
}