package com.retry.task.admin.biz.service;

import com.retry.task.core.executor.AbstractExecutor;
import com.retry.task.core.model.HeartDTO;
import com.retry.task.core.model.RetryTaskDTO;
import com.retry.task.core.model.RetryTaskLogDTO;
import com.retry.task.core.model.base.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/30  00:19
 * @Description TODO
 */
@Service
public class ServerExecutorBizService extends AbstractExecutor {

    @Autowired
    private RetryTaskGroupBizService retryTaskGroupService;

    @Autowired
    private RetryTaskProjectBizService retryTaskProjectService;

    @Autowired
    private RetryTaskBizService retryTaskService;

    @Autowired
    private RetryTaskLogBizService retryTaskLogService;

    @Override
    public Result<String> heart(HeartDTO heartDTO) {
        Result<Long> tokenResult = checkToken(heartDTO.getProjectName(), heartDTO.getIsTest(),
            heartDTO.getToken());
        if (!tokenResult.isSuccess()) {
            return Result.getFail(tokenResult.getCode(), tokenResult.getErrMsg());
        }
        retryTaskGroupService.heartTick(heartDTO);
        return Result.getSuccess("SUCESS");
    }

    @Override
    public Result<Long> createRetryTask(RetryTaskDTO retryTaskDTO) {
        Result<Long> tokenResult = checkToken(retryTaskDTO.getProjectName(),
            retryTaskDTO.getIsTest(), retryTaskDTO.getToken());
        if (!tokenResult.isSuccess()) {return tokenResult;}
        String taskName = retryTaskDTO.getTaskName();
        if (StringUtils.isEmpty(taskName)) {
            return Result.getFail("100", "taskName is null");
        }
        return retryTaskService.createRetryTask(retryTaskDTO);
    }

    private Result<Long> checkToken(String projectName, Integer isTest, String realToken) {
        Result<String> tokenResult = retryTaskProjectService.getRetryTaskProjectToken(projectName,
            isTest);
        if (!tokenResult.isSuccess()) {
            return Result.getFail(tokenResult.getCode(), tokenResult.getErrMsg());
        }
        String token = tokenResult.getData();
        if (!StringUtils.equals(token, realToken)) {
            return Result.getFail("100", "token is not right");
        }
        return Result.getSuccess(null);
    }

    @Override
    public Result<String> createSchedulerTask(RetryTaskDTO retryTaskDTO) {

        return null;
    }

    @Override
    public Result<String> updateRetryTaskLog(RetryTaskLogDTO retryTaskLogDTO) {
        Result<Long> tokenResult = checkToken(retryTaskLogDTO.getProjectName(),
            retryTaskLogDTO.getIsTest(), retryTaskLogDTO.getToken());
        if (!tokenResult.isSuccess()) {
            return Result.getFail(tokenResult.getCode(), tokenResult.getErrMsg());
        }
        Integer type = retryTaskLogDTO.getType();
        if (type.equals(0)) {
            Result<RetryTaskLogDTO> resultDTO = retryTaskLogService.updateLogStartTimeAndTaskStatus(retryTaskLogDTO);
            if (!resultDTO.isSuccess()) {
                return Result.getFail(resultDTO.getCode(), resultDTO.getErrMsg());
            }
            return Result.getSuccess("sucess");
        }
        if (type.equals(1)) {
            Result<RetryTaskLogDTO> resultDTO = retryTaskLogService.updateRetryTaskEndTimeLog(retryTaskLogDTO);
            if (!resultDTO.isSuccess()) {
                return Result.getFail(resultDTO.getCode(), resultDTO.getErrMsg());
            }
            return Result.getSuccess("sucess");
        }
        return Result.getFail("100", "type is not right");
    }
}
