package com.retry.task.admin.constants;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/9  21:05
 * @Description TODO
 */
public enum LockStatusEnum {

    NO_LOCK(1,"未被抢占"),
    LOCKING(2,"占用中");

    private int status;

    private String desc;

    LockStatusEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
