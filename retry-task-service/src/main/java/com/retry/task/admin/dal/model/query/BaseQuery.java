package com.retry.task.admin.dal.model.query;

import java.io.Serializable;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/1  16:19
 * @Description TODO
 */
public class BaseQuery implements Serializable {

    private static final long serialVersionUID = 1324448907429935949L;

    private Integer isTest;



    private Long id;


    private int pageNum = 1;

    private int pageSize = 10;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getOffset() {
        return (pageNum - 1) * pageSize;
    }

    public Integer getIsTest() {
        return isTest;
    }

    public void setIsTest(Integer isTest) {
        this.isTest = isTest;
    }
}
