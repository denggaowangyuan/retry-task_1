package com.retry.task.admin.dal.model;

import java.io.Serializable;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/27  13:12
 * @Description TODO
 */
public class RetryTaskProjectDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = -1222297496076604728L;

    /**
     * 项目名称
     */
    private String projectName;

    private String token;

    private String attribute;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }
}
