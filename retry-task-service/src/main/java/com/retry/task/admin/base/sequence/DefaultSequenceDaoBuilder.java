package com.retry.task.admin.base.sequence;

import com.retry.task.sequence.impl.DefaultSequenceDao;

import javax.sql.DataSource;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/28  15:36
 * @Description TODO
 */
public class DefaultSequenceDaoBuilder {

    private DefaultSequenceDao target;

    private DefaultSequenceDaoBuilder() {
        target = new DefaultSequenceDao();
    }

    public static DefaultSequenceDaoBuilder create() {
        return new DefaultSequenceDaoBuilder();
    }

    public DefaultSequenceDao build() {
        return target;
    }

    public DefaultSequenceDaoBuilder dataSource(DataSource dataSource) {
        target.setDataSource(dataSource);
        return this;
    }

    public DefaultSequenceDaoBuilder retryTimes(int retryTimes) {
        target.setRetryTimes(retryTimes);
        return this;
    }

    public DefaultSequenceDaoBuilder step(int step) {
        target.setStep(step);
        return this;
    }

    public DefaultSequenceDaoBuilder table(String tableName) {
        target.setTableName(tableName);
        return this;
    }

    public DefaultSequenceDaoBuilder nameColumn(String nameColumnName) {
        target.setNameColumnName(nameColumnName);
        return this;
    }

    public DefaultSequenceDaoBuilder valueColumn(String valueColumnName) {
        target.setValueColumnName(valueColumnName);
        return this;
    }

    public DefaultSequenceDaoBuilder gmtModifiedColumn(String gmtModifiedColumnName) {
        target.setGmtModifiedColumnName(gmtModifiedColumnName);
        return this;
    }
}
