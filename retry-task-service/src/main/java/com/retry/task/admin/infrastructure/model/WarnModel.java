package com.retry.task.admin.infrastructure.model;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/9/24  13:49
 * @Description TODO
 */
public class WarnModel {

    /**
     * 告警类型
     */
    private String warnType;

    /**
     * 异常任务名称
     */
    private String taskName;

    /**
     * 异常任务id
     */
    private String taskId;

    /**
     * 告警通知名称
     */
    private String title;

    /**
     * 告警通知内容
     */
    private String text;

    private String param;

    public String getWarnType() {
        return warnType;
    }

    public void setWarnType(String warnType) {
        this.warnType = warnType;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}