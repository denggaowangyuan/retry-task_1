package com.retry.task.admin.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/27  20:38
 * @Description TODO
 */
public class RetryTaskLockDO extends BaseDO implements Serializable {
    private static final long serialVersionUID = 7044314933024335850L;

    private String lockName;

    private Integer lockStatus;

    private Date currentLockTime;

    private String currentLockIp;

    private String requestId;

    private String version;

    /**
     * 超时时间
     */
    private Integer timeOut;



    public Integer getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Integer timeOut) {
        this.timeOut = timeOut;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLockName() {
        return lockName;
    }

    public void setLockName(String lockName) {
        this.lockName = lockName;
    }

    public Integer getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(Integer lockStatus) {
        this.lockStatus = lockStatus;
    }

    public Date getCurrentLockTime() {
        return currentLockTime;
    }

    public void setCurrentLockTime(Date currentLockTime) {
        this.currentLockTime = currentLockTime;
    }

    public String getCurrentLockIp() {
        return currentLockIp;
    }

    public void setCurrentLockIp(String currentLockIp) {
        this.currentLockIp = currentLockIp;
    }
}
