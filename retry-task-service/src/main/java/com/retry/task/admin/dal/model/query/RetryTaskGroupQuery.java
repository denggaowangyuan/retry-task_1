package com.retry.task.admin.dal.model.query;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/13  13:08
 * @Description TODO
 */
public class RetryTaskGroupQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 1818087407539514782L;

    private String projectName;


    //当前时间-5分钟 如果还大于上次心跳，则说明 该项目的ip已经死掉，需要进行删除
    private Date minHeartTime;

    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }


    public Date getMinHeartTime() {
        return minHeartTime;
    }

    public void setMinHeartTime(Date minHeartTime) {
        this.minHeartTime = minHeartTime;
    }
}
