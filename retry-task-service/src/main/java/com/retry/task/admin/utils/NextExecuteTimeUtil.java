package com.retry.task.admin.utils;

import java.util.Date;

import com.retry.task.core.constants.RetryTaskExecuteTypeEnum;
import com.retry.task.core.model.RetryTaskDTO;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/6  16:11
 * @Description TODO
 */
public class NextExecuteTimeUtil {

    public static Date generateFirstTime(RetryTaskDTO retryTaskDTO, Date lastDate) throws Exception {
        Integer executeType = retryTaskDTO.getExecuteType();
        Date nextValidTime = null;
        if (executeType.equals(RetryTaskExecuteTypeEnum.CRON.getType())) {
            nextValidTime = new CronExpression(retryTaskDTO.getCorn()).getNextValidTimeAfter(lastDate);
        }
        if (executeType.equals(RetryTaskExecuteTypeEnum.INTERVAL.getType())) {
            Long firstExecuteTime = lastDate.getTime() + retryTaskDTO.getIntervalSecond() * 1000;
            nextValidTime = new Date(firstExecuteTime);
        }
        return nextValidTime;
    }

}
