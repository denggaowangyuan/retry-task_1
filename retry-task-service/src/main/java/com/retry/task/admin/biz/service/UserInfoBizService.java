package com.retry.task.admin.biz.service;

import com.google.common.collect.Lists;
import com.retry.task.admin.dal.mapper.UserInfoMapper;
import com.retry.task.admin.dal.model.UserInfoDO;
import com.retry.task.admin.dal.model.query.UserInfoQuery;
import com.retry.task.core.model.base.PageResult;
import com.retry.task.core.model.base.Result;
import com.retry.task.core.utils.CommonExecuteUtils;
import com.retry.task.sequence.Sequence;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/12  00:06
 * @Description TODO
 */
@Service
public class UserInfoBizService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private Sequence userInfoSequence;

    @Transactional
    public Result<UserInfoDO> register(UserInfoQuery query) {
        if (query == null) {
            return Result.getFail("100", "query is null");
        }
        if (StringUtils.isEmpty(query.getUserName())) {
            return Result.getFail("100", "userName is null");
        }
        if (StringUtils.isEmpty(query.getPassword())) {
            return Result.getFail("100", "password is null");
        }

        UserInfoDO userInfoDO = new UserInfoDO();
        BeanUtils.copyProperties(query, userInfoDO);
        userInfoDO.setId(userInfoSequence.nextValue());
        userInfoMapper.batchInsert(Lists.newArrayList(userInfoDO));
        return Result.getSuccess(userInfoDO);
    }

    public Result<UserInfoDO> findUserById(Long userId) {
        if (userId == null) {
            return Result.getFail("100", "user info is null");
        }
        UserInfoDO userInfoDO = userInfoMapper.queryByPrimaryKey(userId);
        return Result.getSuccess(userInfoDO);
    }

    public PageResult<List<UserInfoDO>> findUser(UserInfoQuery query) {
        if (query == null) {
            return PageResult.getFail("100", "query is null");
        }

        Map<String, Object> params = CommonExecuteUtils.beanToMap(query, false);
        params.put("limit",query.getPageSize());
        params.put("offset",query.getOffset());
        int count = userInfoMapper.countByCondition(params);
        PageResult<List<UserInfoDO>> pageResult = PageResult.getSuccess(null);
        pageResult.setTotalCount(count);
        if (count == 0) {
            return pageResult;
        }
        List<UserInfoDO> userInfoDOS = userInfoMapper.queryByCondition(params);
        pageResult.setData(userInfoDOS);
        return pageResult;
    }

    public Result<Integer> updateUser(UserInfoQuery query) {
        if (query == null) {
            return Result.getFail("100", "query is null");
        }
        if (query.getId() == null) {
            return Result.getFail("100", "id is null");
        }
        Map<String, Object> params = CommonExecuteUtils.beanToMap(query, false);
        Integer count = userInfoMapper.updateByCondition(params);
        return Result.getSuccess(count);
    }
}
