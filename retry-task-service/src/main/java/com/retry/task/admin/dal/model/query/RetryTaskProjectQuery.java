package com.retry.task.admin.dal.model.query;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/1  16:19
 * @Description TODO
 */
public class RetryTaskProjectQuery extends BaseQuery {


    private String projectName;


    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
