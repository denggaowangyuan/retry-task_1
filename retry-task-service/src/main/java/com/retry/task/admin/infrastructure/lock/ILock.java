package com.retry.task.admin.infrastructure.lock;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/8  22:50
 * @Description TODO 分布式锁实现
 */
public interface ILock {

    boolean tryLock(String key,Integer isTest);

    boolean tryLockWithWait(String key,Integer isTest, Integer waitTime);

    int releasLock(String key,Integer isTest);
}
