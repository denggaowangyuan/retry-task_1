package com.retry.task.admin.dal.model.query;

import java.io.Serializable;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/12  00:23
 * @Description TODO
 */
public class UserInfoQuery extends BaseQuery implements Serializable {
    private static final long serialVersionUID = -6089485196730459235L;


    private String userName;

    private String password;

    private Integer role;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }
}
