package com.retry.task;

import com.retry.task.admin.RetryTaskApplication;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetryTaskApplication.class)
public class Abstract {
}
