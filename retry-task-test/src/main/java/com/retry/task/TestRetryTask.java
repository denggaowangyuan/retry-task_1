package com.retry.task;

import com.retry.task.core.model.RetryTaskContext;
import com.retry.task.core.task.ConsumeStatus;
import com.retry.task.core.task.annotation.RetryTaskAnnotation;
import com.retry.task.core.task.interfaces.AbstractRetryTaskListener;

@RetryTaskAnnotation(projectName = "retry-task",taskName = "test")
public class TestRetryTask extends AbstractRetryTaskListener {

    @Override
    public ConsumeStatus consume(RetryTaskContext retryTaskContext) {
        return super.consume(retryTaskContext);
    }

    @Override
    public void beforeConsume(RetryTaskContext context) {
        super.beforeConsume(context);
    }

    @Override
    public void afterConsume(ConsumeStatus consumeStatus, RetryTaskContext context) {
        super.afterConsume(consumeStatus, context);
    }
}
