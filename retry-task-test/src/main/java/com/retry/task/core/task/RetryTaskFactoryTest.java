package com.retry.task.core.task;


import com.retry.task.Abstract;
import com.retry.task.admin.biz.service.RetryTaskBizService;
import com.retry.task.core.constants.RetryTaskExecuteTypeEnum;
import com.retry.task.core.model.RetryTaskDTO;
import com.retry.task.core.model.base.Result;
import com.retry.task.core.utils.GsonTool;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 集成测试
 */
public class RetryTaskFactoryTest extends Abstract {

    @Autowired
    RetryTaskBizService retryTaskBizService;

    @Test
    public void testCreate() {

        RetryTaskDTO retryTaskDTO =
                RetryTaskDTO.createBuilder()
                        .projectName("retry-task")
                        .isTest(0)
                        .taskName("test")
                        .taskDesc("ceshi")

                        .executeType(RetryTaskExecuteTypeEnum.INTERVAL.getType())
                        .intervalSecond(1000)
                        .parameters("ceshi")
                        .token("retry-task").build();
        Result<Long> result = RetryTaskFactory.getInstance().createRetryTask(retryTaskDTO);
        System.out.println(GsonTool.toJsonString(result));
    }

}