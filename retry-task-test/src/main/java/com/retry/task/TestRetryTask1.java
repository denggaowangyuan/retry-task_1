package com.retry.task;

import com.retry.task.core.model.RetryTaskContext;
import com.retry.task.core.task.ConsumeStatus;
import com.retry.task.core.task.annotation.RetryTaskAnnotation;
import com.retry.task.core.task.interfaces.IRetryTaskListener;

@RetryTaskAnnotation(projectName = "retry-task",taskName = "test1")
public class TestRetryTask1 implements IRetryTaskListener {
    @Override
    public ConsumeStatus consume(RetryTaskContext retryTaskContext) {
        //创建任务是传入的参数
        String params = retryTaskContext.getParameters();
        return null;
    }
}
