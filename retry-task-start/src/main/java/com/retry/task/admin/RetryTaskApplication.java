package com.retry.task.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/20  23:15
 * @Description TODO
 */
@SpringBootApplication(scanBasePackages = {"com.retry.task.admin"})
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class})
@ImportResource({"classpath*:spring/start-spring-context.xml"})
public class RetryTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetryTaskApplication.class, args);
    }
}

