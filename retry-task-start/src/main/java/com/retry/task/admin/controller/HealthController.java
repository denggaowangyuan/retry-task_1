package com.retry.task.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/21  17:19
 * @Description TODO
 */
@Controller
public class HealthController {

    @RequestMapping("/")
    @ResponseBody
    public String health(){
        return "sucess";
    }
}
