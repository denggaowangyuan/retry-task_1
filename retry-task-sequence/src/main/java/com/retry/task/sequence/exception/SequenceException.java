package com.retry.task.sequence.exception;

public class SequenceException extends RuntimeException{

    public SequenceException(Throwable cause) {
        super(cause);
    }

    public SequenceException(String message) {
        super(message);
    }
}
