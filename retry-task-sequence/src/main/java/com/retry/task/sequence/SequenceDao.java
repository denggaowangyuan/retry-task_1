package com.retry.task.sequence;

import com.retry.task.sequence.exception.SequenceException;
import com.retry.task.sequence.lifecycle.Lifecycle;

public interface SequenceDao extends Lifecycle {

    /**
     * 取得下一个可用的序列区间
     *
     * @param name 序列名称
     * @return 返回下一个可用的序列区间
     * @throws SequenceException
     */
    SequenceRange nextRange(String name) throws SequenceException;


    int getStep();

    int getRetryTimes();
}