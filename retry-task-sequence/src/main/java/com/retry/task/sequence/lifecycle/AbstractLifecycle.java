package com.retry.task.sequence.lifecycle;

import com.retry.task.sequence.exception.SequenceException;

public class AbstractLifecycle implements Lifecycle {

    protected final Object lock = new Object();
    protected volatile boolean isInited = false;

    public void init() {
        synchronized (lock) {
            if (isInited()) {
                return;
            }

            try {
                doInit();
                isInited = true;
            } catch (Exception e) {
                // 出现异常调用destory方法，释放
                try {
                    doDestroy();
                } catch (Exception e1) {
                    // ignore
                }
                throw new SequenceException(e);
            }
        }
    }

    public void destroy() {
        synchronized (lock) {
            if (!isInited()) {
                return;
            }

            doDestroy();
            isInited = false;
        }
    }

    public boolean isInited() {
        return isInited;
    }

    protected void doInit() {
    }

    protected void doDestroy() {
    }

}