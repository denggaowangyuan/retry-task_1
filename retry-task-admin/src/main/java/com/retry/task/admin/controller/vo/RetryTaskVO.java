package com.retry.task.admin.controller.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/5  23:20
 * @Description TODO
 */
public class RetryTaskVO implements Serializable {
    private static final long serialVersionUID = -7797494418967587238L;

    private Long id;

    private String gmtCreate;

    private String gmtModified;

    /**
     * 所使用的项目名称
     */
    private String projectName;
    /**
     * 重试任务的描述
     */
    private String taskDesc;

    /**
     * 重试任务的bean 名称
     */
    private String taskName;

    /**
     *
     */
    private Integer taskType;

    /**
     * 执行类型 0 按照intervalSecond 间隔时间
     * 1 cron表达式
     */
    private String executeType;

    /**
     * 是否异步 0 异步
     * 1 同步
     */
    private Integer isAsync;

    /**
     * 是否广播 0单机执行
     * 1 广播
     */
    private Integer isBroadcast;
    /**
     * 执行时间间隔 秒
     */
    private Integer intervalSecond;

    /**
     * 初次创建延迟时间
     */
    private Integer delayTime;
    /**
     * 下次执行时间
     */
    private String nextPlanTime;
    /**
     * 重试次数>1 小于100
     */
    private Integer retryNum;

    private String creatorName;

    private String corn;

    /**
     * 任务预计最大执行时间
     */
    private Integer maxExecuteTime;
    private String creatorId;

    /**
     * 异常通知人
     */
    private String warningPerson;

    /**
     * 通知类型
     */
    private String warningType;

    private String parameters;

    private Long currentLogId;

    private String creatorIp;

    /**
     * 执行执行机器的ip
     */
    private String targetExecuteIp;

    private Integer status;

    private String statusName;

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    private Integer originRetryNum;

    private String attribute;

    private String selectorType;

    private Integer isTest;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getOriginRetryNum() {
        return originRetryNum;
    }

    public void setOriginRetryNum(Integer originRetryNum) {
        this.originRetryNum = originRetryNum;
    }

    public Integer getIsTest() {
        return isTest;
    }

    public void setIsTest(Integer isTest) {
        this.isTest = isTest;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getIntervalSecond() {
        return intervalSecond;
    }

    public void setIntervalSecond(Integer intervalSecond) {
        this.intervalSecond = intervalSecond;
    }

    public Integer getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(Integer delayTime) {
        this.delayTime = delayTime;
    }

    public Integer getRetryNum() {
        return retryNum;
    }

    public void setRetryNum(Integer retryNum) {
        this.retryNum = retryNum;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCorn() {
        return corn;
    }

    public void setCorn(String corn) {
        this.corn = corn;
    }

    public Integer getMaxExecuteTime() {
        return maxExecuteTime;
    }

    public void setMaxExecuteTime(Integer maxExecuteTime) {
        this.maxExecuteTime = maxExecuteTime;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getWarningPerson() {
        return warningPerson;
    }

    public void setWarningPerson(String warningPerson) {
        this.warningPerson = warningPerson;
    }

    public String getWarningType() {
        return warningType;
    }

    public void setWarningType(String warningType) {
        this.warningType = warningType;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getCreatorIp() {
        return creatorIp;
    }

    public void setCreatorIp(String creatorIp) {
        this.creatorIp = creatorIp;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getTargetExecuteIp() {
        return targetExecuteIp;
    }

    public void setTargetExecuteIp(String targetExecuteIp) {
        this.targetExecuteIp = targetExecuteIp;
    }

    public String getSelectorType() {
        return selectorType;
    }

    public void setSelectorType(String selectorType) {
        this.selectorType = selectorType;
    }

    public String getNextPlanTime() {
        return nextPlanTime;
    }

    public void setNextPlanTime(String nextPlanTime) {
        this.nextPlanTime = nextPlanTime;
    }

    public Long getCurrentLogId() {
        return currentLogId;
    }

    public void setCurrentLogId(Long currentLogId) {
        this.currentLogId = currentLogId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTaskType() {
        return taskType;
    }

    public void setTaskType(Integer taskType) {
        this.taskType = taskType;
    }

    public String getExecuteType() {
        return executeType;
    }

    public void setExecuteType(String executeType) {
        this.executeType = executeType;
    }

    public Integer getIsAsync() {
        return isAsync;
    }

    public void setIsAsync(Integer isAsync) {
        this.isAsync = isAsync;
    }

    public Integer getIsBroadcast() {
        return isBroadcast;
    }

    public void setIsBroadcast(Integer isBroadcast) {
        this.isBroadcast = isBroadcast;
    }
}
