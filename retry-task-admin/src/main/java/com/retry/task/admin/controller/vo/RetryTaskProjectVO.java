package com.retry.task.admin.controller.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/5  19:58
 * @Description TODO
 */
public class RetryTaskProjectVO implements Serializable {
    /**
     * 项目名称
     */
    private String projectName;

    private String token;

    private String attribute;

    private Long id;

    /**
     * 创建时间
     *
     * This field corresponds to the database column retry_task_log.gmt_create
     */
    private String gmtCreate;

    /**
     * 修改时间
     *
     * This field corresponds to the database column retry_task_log.gmt_modified
     */
    private String gmtModified;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }
}
