package com.retry.task.admin.controller.param;

import java.io.Serializable;

import com.retry.task.admin.dal.model.query.BaseQuery;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/13  19:56
 * @Description TODO
 */
public class RetryTaskProjectParam extends BaseQuery implements Serializable {
    private static final long serialVersionUID = -230859713625884307L;

    private String projectName;

    private String token;

    private String attribute;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }


}
