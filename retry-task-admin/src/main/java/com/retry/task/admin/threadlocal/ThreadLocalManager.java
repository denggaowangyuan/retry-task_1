package com.retry.task.admin.threadlocal;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import org.reflections.Reflections;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/13  14:55
 * @Description TODO
 */
public class ThreadLocalManager extends AbtractThreadLocal {
    private static List<AbtractThreadLocal> threadLocalList = Lists.newArrayList();

    private static class InnerClass {
        private static ThreadLocalManager threadLocalManager = new ThreadLocalManager();
    }

    static {
        Reflections reflection = new Reflections(AbtractThreadLocal.class.getPackage().getName());

        Set<Class<? extends AbtractThreadLocal>> classSet = reflection.getSubTypesOf(AbtractThreadLocal.class);
        for (Class<? extends AbtractThreadLocal> test : classSet) {
            try {
                AbtractThreadLocal threadLocal = test.newInstance();
                threadLocalList.add(threadLocal);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }


    public static ThreadLocalManager getInstance() {
        return InnerClass.threadLocalManager;
    }

    @Override
   public void removeThreadLocal() {
        for (AbtractThreadLocal abtractThreadLocal : threadLocalList) {
            if (!(abtractThreadLocal instanceof ThreadLocalManager)) {
                abtractThreadLocal.removeThreadLocal();
            }
        }
    }

}
