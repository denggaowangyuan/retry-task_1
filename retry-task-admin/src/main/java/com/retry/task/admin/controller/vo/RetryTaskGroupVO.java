package com.retry.task.admin.controller.vo;

import java.io.Serializable;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/4/27  13:12
 * @Description TODO
 */
public class RetryTaskGroupVO implements Serializable {

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 机器ip
     */
    private String executeIp;

    /**
     * 机器类型
     */
    private Integer executeType;

    private String heartTime;

    /**
     * 创建时间
     *
     * This field corresponds to the database column retry_task_log.gmt_create
     */
    private String gmtCreate;

    /**
     * 修改时间
     *
     * This field corresponds to the database column retry_task_log.gmt_modified
     */
    private String gmtModified;
    /**
     * /**
     * 心跳次数
     */
    private Long heartTick;

    /**
     * 活动级别
     */
    private Integer activeLevel;

    private Integer status;

    private String statusName;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getExecuteIp() {
        return executeIp;
    }

    public void setExecuteIp(String executeIp) {
        this.executeIp = executeIp;
    }

    public Integer getExecuteType() {
        return executeType;
    }

    public void setExecuteType(Integer executeType) {
        this.executeType = executeType;
    }

    public String getHeartTime() {
        return heartTime;
    }

    public void setHeartTime(String heartTime) {
        this.heartTime = heartTime;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Long getHeartTick() {
        return heartTick;
    }

    public void setHeartTick(Long heartTick) {
        this.heartTick = heartTick;
    }

    public Integer getActiveLevel() {
        return activeLevel;
    }

    public void setActiveLevel(Integer activeLevel) {
        this.activeLevel = activeLevel;
    }
}
