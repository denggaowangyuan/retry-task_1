package com.retry.task.admin.test;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.retry.task.core.model.RetryTaskContext;
import com.retry.task.core.task.ConsumeStatus;
import com.retry.task.core.task.annotation.RetryTaskAnnotation;
import com.retry.task.core.task.interfaces.AbstractRetryTaskListener;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/11  14:55
 * @Description TODO
 */
@RetryTaskAnnotation(projectName = "retry-task", taskName = "test-5")
public class Test5 extends AbstractRetryTaskListener {

    private int tick = 0;

    @Override
    public ConsumeStatus consume(RetryTaskContext retryTaskContext) {
        System.out.println("=================");
        tick++;
        try {
            Random random = new Random();
            TimeUnit.SECONDS.sleep(random.nextInt(10));
        } catch (Exception ex) {

        }

        if (tick == 20) {
            throw new RuntimeException("异常测试");
        }
        return ConsumeStatus.FAIL;
    }

    @Override
    public void beforeConsume(RetryTaskContext context) {
        System.out.println("========before=========");
    }

    @Override
    public void afterConsume(ConsumeStatus consumeStatus, RetryTaskContext context) {
        System.out.println("========after=========");
    }
}
