package com.retry.task.admin.advice;

import java.lang.reflect.UndeclaredThrowableException;

import com.retry.task.core.model.base.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/13  16:25
 * @Description TODO
 */
@ControllerAdvice(basePackages = "com.retry.task.admin.controller")
public class ControllerExceptionAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionAdvice.class);

    @ExceptionHandler({Exception.class})
    public Result handleBizException(Exception ex) {

        String errMsg = ex.getMessage();
        if (errMsg == null) {
            errMsg = "unknown exception ,pleas linked admin";
        }
        LOGGER.error("[异常处理]错误信息为:{},异常原因:{}", errMsg, ex.getMessage(), ex);
        return Result.getFail("100", errMsg);
    }
}
