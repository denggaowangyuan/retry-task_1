package com.retry.task.admin.config;

import com.retry.task.admin.filter.LoginFilter;
import com.retry.task.admin.biz.service.UserInfoBizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/12  23:26
 * @Description TODO
 */
@Configuration
public class FilterConfigure {

    @Autowired
    private UserInfoBizService userInfoService;

    @Bean
    public FilterRegistrationBean buildLoginFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        LoginFilter loginFilter = new LoginFilter();

        filterRegistrationBean.setOrder(2);
        filterRegistrationBean.setFilter(loginFilter);
        filterRegistrationBean.setName("loginFilter");
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter(LoginFilter.WHITE_LIST,"*.js,*.html,*.css,*.woff2,*.jpg,*.png");
        filterRegistrationBean.addInitParameter(LoginFilter.LOGIN_API, "/retrytask/login");
        filterRegistrationBean.addInitParameter(LoginFilter.LOGIN_URL, "/admin/login.html");
        loginFilter.setUserInfoService(userInfoService);
        return filterRegistrationBean;
    }

}
