package com.retry.task.admin.controller;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import com.google.common.collect.Lists;
import com.retry.task.core.model.base.PageResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/18  21:27
 * @Description TODO
 */
public class BaseController {

    @Value("${envirment.isTest}")
    protected Integer isTest;

    protected <S, O> PageResult<List<O>> convertObject(PageResult<List<S>> sourceResult, Function<S, O> function) {
        if (!sourceResult.isSuccess()) {
            return PageResult.getFail(sourceResult.getCode(), sourceResult.getErrMsg());
        }
        PageResult<List<O>> finalResult = PageResult.getSuccess(null);
        int totalCount = sourceResult.getTotalCount();
        finalResult.setTotalCount(totalCount);
        if (totalCount == 0) {
            return finalResult;
        }
        List<S> sList = sourceResult.getData();

        if (CollectionUtils.isEmpty(sList)) {
            return finalResult;
        }
        List<O> oList = Lists.newArrayList();
        for (S s : sList) {
            O obj = function.apply(s);
            oList.add(obj);
        }
        finalResult.setData(oList);
        return finalResult;
    }
}
