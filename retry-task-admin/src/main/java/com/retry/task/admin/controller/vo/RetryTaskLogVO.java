package com.retry.task.admin.controller.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/8  17:24
 * @Description TODO
 */
public class RetryTaskLogVO implements Serializable {
    private static final long serialVersionUID = 5815428554190935287L;

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Integer isTest;
    private String projectName;

    private String taskName;

    private Long taskId;

    /**
     * 创建时间
     *
     * This field corresponds to the database column retry_task_log.gmt_create
     */
    private String gmtCreate;

    /**
     * 修改时间
     *
     * This field corresponds to the database column retry_task_log.gmt_modified
     */
    private String gmtModified;
    /**
     * 预计开始时间
     */
    private String preStartTime;

    private String statusName;

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    /**
     * 实际开始时间
     */
    private String startTime;

    /**
     * 执行结束时间
     */
    private String endTime;

    /**
     * 执行轨迹id
     */
    private String traceId;

    /**
     * 执行机器ip
     */
    private String executeIp;

    /**
     * 异常出错信息
     */
    private String errorMessage;

    private Integer status;

    private String token;

    private Integer type;

    public Integer getIsTest() {
        return isTest;
    }

    public void setIsTest(Integer isTest) {
        this.isTest = isTest;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getPreStartTime() {
        return preStartTime;
    }

    public void setPreStartTime(String preStartTime) {
        this.preStartTime = preStartTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getExecuteIp() {
        return executeIp;
    }

    public void setExecuteIp(String executeIp) {
        this.executeIp = executeIp;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
