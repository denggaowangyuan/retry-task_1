package com.retry.task.admin.session;

import java.io.Serializable;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/12  23:18
 * @Description TODO
 */
public class SessionInfo implements Serializable {

    private static final long serialVersionUID = -5513651025877528233L;
    private String userName;

    private Long id;

    private Integer role;

    private String projectId;

    /**
     * 上次登录时间
     */
    private Long lastTime;

    public Long getLastTime() {
        return lastTime;
    }

    public void setLastTime(Long lastTime) {
        this.lastTime = lastTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SessionInfo{");
        sb.append("userName='").append(userName).append('\'');
        sb.append(", id=").append(id);
        sb.append(", role=").append(role);
        sb.append(", projectId='").append(projectId).append('\'');
        sb.append(", lastTime=").append(lastTime);
        sb.append('}');
        return sb.toString();
    }
}
