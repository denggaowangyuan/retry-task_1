package com.retry.task.admin.controller.param;

import java.io.Serializable;
import java.util.Date;

import com.retry.task.admin.dal.model.query.BaseQuery;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/21  23:47
 * @Description TODO
 */
public class RetryTaskLogParam extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 5591193014653751928L;

    private Long taskId;

    private String executeIp;

    private Integer status;

    private Date minPreStartTime;
    private Date maxPreStartTime;
    private Date minStartTime;
    private Date maxStartTime;
    private Date minEndTime;
    private Date maxEndTime;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getMinPreStartTime() {
        return minPreStartTime;
    }

    public void setMinPreStartTime(Date minPreStartTime) {
        this.minPreStartTime = minPreStartTime;
    }

    public Date getMaxPreStartTime() {
        return maxPreStartTime;
    }

    public void setMaxPreStartTime(Date maxPreStartTime) {
        this.maxPreStartTime = maxPreStartTime;
    }

    public Date getMinStartTime() {
        return minStartTime;
    }

    public void setMinStartTime(Date minStartTime) {
        this.minStartTime = minStartTime;
    }

    public Date getMaxStartTime() {
        return maxStartTime;
    }

    public void setMaxStartTime(Date maxStartTime) {
        this.maxStartTime = maxStartTime;
    }

    public Date getMinEndTime() {
        return minEndTime;
    }

    public void setMinEndTime(Date minEndTime) {
        this.minEndTime = minEndTime;
    }

    public Date getMaxEndTime() {
        return maxEndTime;
    }

    public void setMaxEndTime(Date maxEndTime) {
        this.maxEndTime = maxEndTime;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getExecuteIp() {
        return executeIp;
    }

    public void setExecuteIp(String executeIp) {
        this.executeIp = executeIp;
    }
}
