package com.retry.task.admin.threadlocal;

import javax.servlet.http.HttpServletRequest;

import com.retry.task.admin.session.SessionInfo;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/12  23:16
 * @Description TODO
 */
public class SessionThreadLocalService extends AbtractThreadLocal {

    private static final ThreadLocal<SessionInfo> SESSION_INFO_THREAD_LOCAL = new ThreadLocal<>();

    private static final ThreadLocal<HttpServletRequest> HTTP_SERVLET_REQUEST_THREAD_LOCAL = new ThreadLocal<>();

    public static void setSession(SessionInfo sessionInfo) {
        SESSION_INFO_THREAD_LOCAL.set(sessionInfo);
    }

    public static SessionInfo getSessionInfo() {
        return SESSION_INFO_THREAD_LOCAL.get();
    }

    public static void removeSessionInfo() {
        SESSION_INFO_THREAD_LOCAL.remove();
    }

    public static void setRequest(HttpServletRequest request) {
        HTTP_SERVLET_REQUEST_THREAD_LOCAL.set(request);
    }

    public static HttpServletRequest getRequest() {
        return HTTP_SERVLET_REQUEST_THREAD_LOCAL.get();
    }

    public static void removeRequest() {
        HTTP_SERVLET_REQUEST_THREAD_LOCAL.remove();
    }

    @Override
    public void removeThreadLocal() {
        HTTP_SERVLET_REQUEST_THREAD_LOCAL.remove();
        SESSION_INFO_THREAD_LOCAL.remove();
    }
}
