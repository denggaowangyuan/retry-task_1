package com.retry.task.admin.controller;

import com.retry.task.admin.controller.param.RetryTaskProjectParam;
import com.retry.task.admin.controller.vo.RetryTaskGroupVO;
import com.retry.task.admin.controller.vo.RetryTaskProjectVO;
import com.retry.task.admin.biz.service.RetryTaskGroupBizService;
import com.retry.task.admin.biz.service.RetryTaskProjectBizService;
import com.retry.task.admin.dal.model.RetryTaskGroupDO;
import com.retry.task.admin.dal.model.RetryTaskProjectDO;
import com.retry.task.admin.dal.model.query.RetryTaskGroupQuery;
import com.retry.task.admin.dal.model.query.RetryTaskProjectQuery;
import com.retry.task.admin.utils.DateUtils;
import com.retry.task.core.model.base.PageResult;
import com.retry.task.core.model.base.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/13  19:48
 * @Description TODO
 */
@RequestMapping("/retrytask/project")
@Controller
public class RetryTaskProjectController extends BaseController {

    @Autowired
    private RetryTaskProjectBizService retryTaskProjectService;

    @Autowired
    private RetryTaskGroupBizService retryTaskGroupService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public Result<String> registerTaskProject(@RequestBody RetryTaskProjectParam retryTaskProjectParam) {
        String projectName = retryTaskProjectParam.getProjectName();
        if (StringUtils.isEmpty(projectName)) {
            return Result.getFail("100", "projectName is null");
        }
        if (StringUtils.isEmpty(retryTaskProjectParam.getToken())) {
            return Result.getFail("100", "token is null");
        }
        RetryTaskProjectQuery projectQuery = new RetryTaskProjectQuery();
        BeanUtils.copyProperties(retryTaskProjectParam, projectQuery);
        projectQuery.setIsTest(isTest);
        PageResult<List<RetryTaskProjectDO>> pageResult = retryTaskProjectService.getRetryTaskProject(projectQuery);
        List<RetryTaskProjectDO> retryTaskProjectDOList = pageResult.getData();
        if (!CollectionUtils.isEmpty(retryTaskProjectDOList)) {
            return Result.getFail("100", "project is already exists");
        }
        RetryTaskProjectDO retryTaskProjectDO = new RetryTaskProjectDO();
        retryTaskProjectDO.setIsTest(isTest);
        retryTaskProjectDO.setProjectName(projectName);
        retryTaskProjectDO.setAttribute(retryTaskProjectParam.getAttribute());
        retryTaskProjectDO.setToken(retryTaskProjectParam.getToken());
        Result<RetryTaskProjectDO> insertResult = retryTaskProjectService.insertRetryTaskProject(retryTaskProjectDO);
        if (!insertResult.isSuccess()) {
            return Result.getFail(insertResult.getCode(), insertResult.getErrMsg());
        }
        return Result.getSuccess("SUCESS");
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public Result<Integer> updateTaskProject(@RequestBody RetryTaskProjectParam retryTaskProjectParam) {
        Long id = retryTaskProjectParam.getId();
        if (id == null) {
            return Result.getFail("100", "id is null");
        }
        RetryTaskProjectDO retryTaskProjectDO = new RetryTaskProjectDO();
        retryTaskProjectDO.setIsTest(isTest);
        BeanUtils.copyProperties(retryTaskProjectParam, retryTaskProjectDO);
        //projectName 不变更
        retryTaskProjectDO.setProjectName(null);
        Result<Integer> updateResult = retryTaskProjectService.updateByRetryTaskProjectId(retryTaskProjectDO);
        if (!updateResult.isSuccess()) {
            return Result.getFail(updateResult.getCode(), updateResult.getErrMsg());
        }
        return Result.getSuccess(updateResult.getData());
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    public Result<Integer> delete(@RequestParam("id") Long id) {
        if (id == null) {
            return Result.getFail("100", "id is null");
        }
        return retryTaskProjectService.deleteByRetryTaskProjectId(id);
    }



    @RequestMapping(value = "/queryRetryTaskGroup", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<List<RetryTaskGroupVO>> queryRetryTaskGroup(String projectName
        , Integer pageSize, Integer pageNum) {
        if (StringUtils.isEmpty(projectName)) {
            return PageResult.getFail("100", "projectName is null or empty");
        }
        if (pageNum <= 0) {
            pageNum = 1;
        }
        if (pageSize <= 0 || pageSize > 20) {
            pageSize = 10;
        }
        RetryTaskGroupQuery query = new RetryTaskGroupQuery();
        query.setIsTest(isTest);
        query.setProjectName(projectName);
        query.setPageSize(pageSize);
        query.setPageNum(pageNum);
        PageResult<List<RetryTaskGroupDO>> pageResult = retryTaskGroupService.queryByRetryTaskGroupByPage(query);
        return convertObject(pageResult, retryTaskGroupDO -> {
            RetryTaskGroupVO retryTaskGroupVO = new RetryTaskGroupVO();
            BeanUtils.copyProperties(retryTaskGroupDO, retryTaskGroupVO);
            String statusName = retryTaskGroupVO.getStatus() == 1 ? "正常" : "超过五分钟未心跳";
            retryTaskGroupVO.setGmtCreate(
                DateUtils.format(retryTaskGroupDO.getGmtCreate(), DateUtils.DATE_TIME_FORMAT));
            retryTaskGroupVO.setGmtModified(
                DateUtils.format(retryTaskGroupDO.getGmtModified(), DateUtils.DATE_TIME_FORMAT));
            if (retryTaskGroupDO.getHeartTime() != null) {
                retryTaskGroupVO.setHeartTime(
                    DateUtils.format(retryTaskGroupDO.getHeartTime(), DateUtils.DATE_TIME_FORMAT));
            }
            retryTaskGroupVO.setStatusName(statusName);
            return retryTaskGroupVO;
        });
    }

    @RequestMapping(value = "/queryRetryTaskProject", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<List<RetryTaskProjectVO>> queryRetryTaskProject(RetryTaskProjectParam retryTaskProjectParam) {
        if (retryTaskProjectParam.getPageNum() <= 0) {
            retryTaskProjectParam.setPageNum(1);
        }
        if (retryTaskProjectParam.getPageSize() >= 30) {
            retryTaskProjectParam.setPageSize(30);
        }
        RetryTaskProjectQuery projectQuery = new RetryTaskProjectQuery();
        BeanUtils.copyProperties(retryTaskProjectParam, projectQuery);
        projectQuery.setIsTest(isTest);
        PageResult<List<RetryTaskProjectDO>> result = retryTaskProjectService.getRetryTaskProject(projectQuery);

        return convertObject(result, retryTaskProjectDO -> {
            RetryTaskProjectVO projectVO = new RetryTaskProjectVO();
            BeanUtils.copyProperties(retryTaskProjectDO, projectVO);
            projectVO.setGmtCreate(DateUtils.format(retryTaskProjectDO.getGmtCreate(), DateUtils.DATE_TIME_FORMAT));
            projectVO.setGmtModified(DateUtils.format(retryTaskProjectDO.getGmtModified(), DateUtils.DATE_TIME_FORMAT));
            return projectVO;
        });
    }

}
