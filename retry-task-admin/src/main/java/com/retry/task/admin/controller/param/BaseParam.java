package com.retry.task.admin.controller.param;

import java.io.Serializable;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/21  00:28
 * @Description TODO
 */
public class BaseParam implements Serializable {
    private static final long serialVersionUID = -577431582402526593L;

    private Long id;



    private int page = 1;

    private int limit = 10;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
