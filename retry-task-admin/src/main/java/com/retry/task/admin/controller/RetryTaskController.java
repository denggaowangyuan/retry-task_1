package com.retry.task.admin.controller;

import java.util.List;

import com.google.common.collect.Lists;
import com.retry.task.admin.controller.param.RetryTaskLogParam;
import com.retry.task.admin.controller.param.RetryTaskParam;
import com.retry.task.admin.controller.vo.RetryTaskLogVO;
import com.retry.task.admin.controller.vo.RetryTaskVO;
import com.retry.task.admin.infrastructure.scheduler.RetryTaskScheduler;
import com.retry.task.admin.biz.service.RetryTaskLogBizService;
import com.retry.task.admin.biz.service.RetryTaskBizService;
import com.retry.task.admin.dal.model.RetryTaskDO;
import com.retry.task.admin.dal.model.query.RetryTaskLogQuery;
import com.retry.task.admin.dal.model.query.RetryTaskQuery;
import com.retry.task.admin.utils.DateUtils;
import com.retry.task.core.constants.RetryTaskExecuteTypeEnum;
import com.retry.task.core.constants.RetryTaskStatusEnum;
import com.retry.task.core.model.RetryTaskDTO;
import com.retry.task.core.model.RetryTaskLogDTO;
import com.retry.task.core.model.base.PageResult;
import com.retry.task.core.model.base.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/7  13:48
 * @Description TODO
 */
@RequestMapping("/retrytask/task")
@Controller
public class RetryTaskController extends BaseController {
    @Autowired
    private RetryTaskBizService retryTaskService;

    @Autowired
    private RetryTaskLogBizService retryTaskLogService;

    @Autowired
    private RetryTaskScheduler retryTaskScheduler;

    @RequestMapping(value = "/triggerTask", method = RequestMethod.GET)
    @ResponseBody
    public Result<String> triggerTask(@RequestParam("taskId") Long taskId) {
        RetryTaskQuery query = new RetryTaskQuery();
        query.setIsTest(isTest);
        query.setIdList(Lists.newArrayList(taskId));
        query.setStatus(RetryTaskStatusEnum.TO_START.getCode());
        PageResult<List<RetryTaskDTO>> pageResult = retryTaskService.queryRetryTask(query);
        if (!pageResult.isSuccess()) {
            return Result.getFail(pageResult.getCode(), pageResult.getErrMsg());
        }
        List<RetryTaskDTO> taskDTOList = pageResult.getData();
        if (CollectionUtils.isEmpty(taskDTOList)) {
            return Result.getFail("100", "can not find to start task by id");
        }
        retryTaskScheduler.sendRetryTask(taskDTOList);
        return Result.getSuccess("trigger sucess");
    }

    @RequestMapping(value = "/queryRetryTask", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<List<RetryTaskVO>> queryRetryTask(RetryTaskParam retryTaskParam) {
        RetryTaskQuery retryTaskQuery = new RetryTaskQuery();
        BeanUtils.copyProperties(retryTaskParam, retryTaskQuery);
        retryTaskQuery.setIsTest(isTest);
        PageResult<List<RetryTaskDO>> pageResult = retryTaskService.queryRetryTaskDO(retryTaskQuery);
        return convertObject(pageResult, retryTaskDO -> {
            RetryTaskVO retryTaskVO = new RetryTaskVO();
            BeanUtils.copyProperties(retryTaskDO, retryTaskVO);
            retryTaskVO.setGmtCreate(DateUtils.format(retryTaskDO.getGmtCreate(), DateUtils.DATE_TIME_FORMAT));
            retryTaskVO.setGmtModified(DateUtils.format(retryTaskDO.getGmtModified(), DateUtils.DATE_TIME_FORMAT));
            if (retryTaskDO.getNextPlanTime() != null) {
                retryTaskVO.setNextPlanTime(
                    DateUtils.format(retryTaskDO.getNextPlanTime(), DateUtils.DATE_TIME_FORMAT));
            }
            RetryTaskStatusEnum retryTaskStatusEnum = RetryTaskStatusEnum.get(retryTaskDO.getStatus());
            if (retryTaskStatusEnum != null) {
                retryTaskVO.setStatusName(retryTaskStatusEnum.getDesc());
            }
            RetryTaskExecuteTypeEnum retryTaskExecuteTypeEnum = RetryTaskExecuteTypeEnum.get(
                retryTaskDO.getExecuteType());
            if (retryTaskExecuteTypeEnum != null) {
                retryTaskVO.setExecuteType(retryTaskExecuteTypeEnum.getDesc());
                if (retryTaskExecuteTypeEnum.equals(RetryTaskExecuteTypeEnum.CRON)) {
                    retryTaskVO.setIntervalSecond(null);
                }
            }
            return retryTaskVO;
        });
    }

    @RequestMapping(value = "/queryRetryTaskLog", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<List<RetryTaskLogVO>> queryRetryTaskLog(RetryTaskLogParam logParam) {
        if (logParam.getTaskId() == null) {
            return PageResult.getFail("100", "taskId is null");
        }
        RetryTaskLogQuery query = new RetryTaskLogQuery();
        BeanUtils.copyProperties(logParam, query);

        PageResult<List<RetryTaskLogDTO>> pageResult = retryTaskLogService.queryRetryTaskLog(query);

        return convertObject(pageResult, retryTaskLogDTO -> {
            RetryTaskLogVO retryTaskVO = new RetryTaskLogVO();
            BeanUtils.copyProperties(retryTaskLogDTO, retryTaskVO);
            retryTaskVO.setGmtCreate(DateUtils.format(retryTaskLogDTO.getGmtCreate(), DateUtils.DATE_TIME_FORMAT));
            retryTaskVO.setGmtModified(DateUtils.format(retryTaskLogDTO.getGmtModified(), DateUtils.DATE_TIME_FORMAT));
            if (retryTaskLogDTO.getPreStartTime() != null) {
                retryTaskVO.setPreStartTime(
                    DateUtils.format(retryTaskLogDTO.getPreStartTime(), DateUtils.DATE_TIME_FORMAT));
            }
            if (retryTaskLogDTO.getStartTime() != null) {
                retryTaskVO.setStartTime(DateUtils.format(retryTaskLogDTO.getStartTime(), DateUtils.DATE_TIME_FORMAT));
            }
            if (retryTaskLogDTO.getEndTime() != null) {
                retryTaskVO.setEndTime(DateUtils.format(retryTaskLogDTO.getEndTime(), DateUtils.DATE_TIME_FORMAT));
            }
            RetryTaskStatusEnum retryTaskStatusEnum = RetryTaskStatusEnum.get(retryTaskLogDTO.getStatus());
            if (retryTaskStatusEnum != null) {
                retryTaskVO.setStatusName(retryTaskStatusEnum.getDesc());
            }
            return retryTaskVO;
        });

    }
}
