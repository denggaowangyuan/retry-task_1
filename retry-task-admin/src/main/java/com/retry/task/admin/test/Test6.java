package com.retry.task.admin.test;

import com.retry.task.core.model.RetryTaskContext;
import com.retry.task.core.task.ConsumeStatus;
import com.retry.task.core.task.annotation.RetryTaskAnnotation;
import com.retry.task.core.task.interfaces.AbstractRetryTaskListener;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/11  14:55
 * @Description TODO
 */
@RetryTaskAnnotation(projectName = "retry-task", taskName = "test-6")
public class Test6 extends AbstractRetryTaskListener {

    @Override
    public ConsumeStatus consume(RetryTaskContext retryTaskContext) {
        System.out.println("=================");
        return ConsumeStatus.FAIL;
    }

    @Override
    public void beforeConsume(RetryTaskContext context) {
        System.out.println("========before=========");
    }

    @Override
    public void afterConsume(ConsumeStatus consumeStatus, RetryTaskContext context) {
        System.out.println("========after=========");
    }
}
