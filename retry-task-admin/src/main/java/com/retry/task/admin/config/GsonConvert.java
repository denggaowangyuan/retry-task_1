package com.retry.task.admin.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/6/9  16:20
 * @Description TODO
 */
@Configuration
public class GsonConvert {

    @Bean
    @ConditionalOnMissingBean
    GsonHttpMessageConverter gsonHttpMessageConverter() {
        GsonHttpMessageConverter converter = new GsonHttpMessageConverter();
        Gson gson = new GsonBuilder().serializeNulls().create();
        converter.setGson(gson);
        return converter;
    }
}
