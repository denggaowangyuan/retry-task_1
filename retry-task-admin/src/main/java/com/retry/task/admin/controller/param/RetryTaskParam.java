package com.retry.task.admin.controller.param;

import java.io.Serializable;

import com.retry.task.admin.dal.model.query.BaseQuery;

/**
 * @author gao.gwq
 * @version 1.0
 * @date 2022/5/21  23:34
 * @Description TODO
 */
public class RetryTaskParam extends BaseQuery implements Serializable {
    private static final long serialVersionUID = 8355752650043525117L;

    private String projectName;

    private Long id;

    private Integer taskType;

    private Integer status;

    private String taskName;

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTaskType() {
        return taskType;
    }

    public void setTaskType(Integer taskType) {
        this.taskType = taskType;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
