异常任务重试管理平台是一个中心化的，重试任务中心。让重试任务和定时任务的接入更加灵活方便。
**
![img.png](img.png)
项目结构
** 项目结构。
retry-task-admin  任务管理中心管理项目。
retry-task-service 任务重试中心服务项目。
retry-task-core   任务管理中心核心项目。其他项目如果要接入任务中心，需要集成当二方包
retry-task-service 任务重试中心服务项目。
retry-task-extension 扩展点，支持spring扩展点。
retry-task-datasource 多数据源。
retry-task-sequence id生成器。支持生产mysql主键。借鉴了tddl-sequence的id实现方式。
retry-task-start 项目启动类。

项目依赖关系
输入图片说明
![img_1.png](img_1.png)
底层使用netty server。使用http进行通信

项目架构：
![img_3.png](img_3.png)
业务接入入说明：
1:接入方需要引入retry-task-core二方包。
接入应用中的配置中需要添加如下配置。
`spring.retry.task.enable=true （缺省是为true，为false时，启动不会加载相关配置项）
spring.retry.task.serverHost=http://127.0.0.1:9999 （server 服务域名，多个时可以使用逗号分隔）
spring.retry.task.projectName=retry-task （接入项目名称）
spring.retry.task.token=retry-task （接入项目对应的token）
spring.retry.task.clientPort=9997 （接入项目port。）

#spring.retry.task.clientIp=	http://retrytask.com （接入项目的ip地址。如果不配置该项，默认取当前的机器的ip地址）
`
管理页面需要将接入的项目注册到系统中。
输入图片说明

接入的代码：
创建重试任务
`
    RetryTaskDTO retryTaskDTO = RetryTaskDTO.
                createBuilder().
                projectName("retry-task").//接入项目名称
                isTest(0).//环境标识 根据自己环境的不同进行设置
                taskName("test").//执行任务的名称
                taskDesc("ceshi").//任务描述
                executeType(RetryTaskExecuteTypeEnum.INTERVAL.getType()).//任务类型，按照时间间隔或者cron表达式
                intervalSecond(1000).//时间间隔
                parameters("ceshi").//任务参数
                token("retry-task").//任务的token和配置中的相同。
                build(); 
    Result<Long> result = RetryTaskFactory.getInstance().createRetryTask(retryTaskDTO); 
`
项目接入：
    提供两种实现方案
    实现IRetryTaskListener接口。
    `@RetryTaskAnnotation(projectName = "retry-task",taskName = "test1")
    public class TestRetryTask1 implements IRetryTaskListener {
    
        @Override
        public ConsumeStatus consume(RetryTaskContext retryTaskContext) {
            //创建任务是传入的参数
            String params = retryTaskContext.getParameters();
            return null;
        }
    }`


    或者继承AbstractRetryTaskListener
`@RetryTaskAnnotation(projectName = "retry-task",taskName = "test")
public class TestRetryTask extends AbstractRetryTaskListener {

    @Override
    public ConsumeStatus consume(RetryTaskContext retryTaskContext) {
        return super.consume(retryTaskContext);
    }

    @Override
    public void beforeConsume(RetryTaskContext context) {
        super.beforeConsume(context);
    }

    @Override
    public void afterConsume(ConsumeStatus consumeStatus, RetryTaskContext context) {
        super.afterConsume(consumeStatus, context);
    }
}`
当前项目可以直接应用到线上。如有使用，请联系作者本人，在线上使用的过程出现的问题。本人会逐渐进行优化